#!/usr/bin/env python
'''
`steps.py` consists of largely refactored legacy code. 
More or less, each function in `steps.py` started its life as a standalone python script, with its own argument handling and way of interacting with the cluster. 
Now, each function largely consists of logic needed to construct individual data processing jobs. 

Each job consists of a script, located in `runscript`. These scripts are also largely refactored legacy code. 
Most are extremely inflexible and should not be run as standalone scripts in a normal use case. However, they can be run as standalone scripts for debugging purposes. These scripts are designed to log each and every subprocess used for data processing, and to fail if any subroutine fails. In addition to creating job specs, these functions create rmfile lists, which are lists of files to be destroyed, along with the last step in which they are needed.

'''
import json
import shutil
import time
import os
import sys
import csv
import re
import logging
import glob
import pickle
import subprocess as sp
import importlib
import tempfile
import datetime
import collections
import iproc.commons as commons
from iproc.bids import sanitize,split_task

#get logger from calling script
logger = logging.getLogger(__name__)
JobSpec = commons.JobSpec

class jobConstructor(object):
    def __init__(self, conf,scans,args):
        self.conf = conf
        self.scans = scans
        self.args = args
        # TODO: remove moribund function
        self.steplog_base = os.path.join(conf.iproc.LOGDIR,'stepLog')
        self.rmfiles = {}
        self.rm_dump_filename = os.path.join(self.conf.iproc.RMFILE_DUMP,'{STAGE}.crash'.format(STAGE=self.args.stage))
        self.rm_final_filename = os.path.join(self.conf.iproc.RMFILE_DUMP,'{STAGE}.final'.format(STAGE=self.args.stage))

    def reset_steplog(self):
        # TODO: remove moribund function
        self.steplog_base = os.path.join(self.conf.iproc.LOGDIR,'stepLog')

    def anat_from_bids(self,overwrite=True):
        stepname_base = 'anat_from_bids'
        logger.debug(stepname_base)

        self.reset_steplog()
        job_spec_list = []
        self.reset_steplog()
        for sessionid,sess in self.scans.anat_sessions():
            sub = self.conf.iproc.SUB
            ses = sessionid
            run = sess.bids_anat_no
            logger.info('processing sub=%s, ses=%s, anat=%s', sub, ses, run)
            basename = 'ses-{ses}/anat/sub-{sub}_ses-{ses}_run-{run}_T1w.nii.gz'
            basename = basename.format(sub=sanitize(sub),
                                       ses=sanitize(ses),
                                       run=run)
            bids_anat_file = os.path.join(self.args.bids, basename)
            if not os.path.exists(bids_anat_file):
                raise IOError('{} does not exist.'.format(bids_anat_file))
            run_zpad = '{0:03d}'.format(int(sess.anat_no)) # note that this is the ScanNumber, not the BIDS run number
            anat_basename = '{0}_mpr{1}'.format(ses, run_zpad)
            anat_dirname = os.path.join(self.conf.iproc.NATDIR, ses, 'ANAT')
            work_dirname = os.path.join(self.conf.iproc.WORKDIR, 'ANAT_{0}'.format(ses))
            dest_nii = os.path.join(anat_dirname, anat_basename + '.nii.gz')
            dest_reorient_nii = os.path.join(anat_dirname, anat_basename + '_reorient.nii.gz')
            outfiles = [dest_reorient_nii]
            if self._outfiles_skip(overwrite,outfiles):
                continue
            elif not self.args.no_remove_files:
                for outfile in outfiles:
                    os.remove(outfile) 
                    logging.debug('removed {}'.format(outfile))

            for d in [anat_dirname, work_dirname]:
                if not os.path.exists(d):
                    os.makedirs(d)
            script = os.path.join(os.path.expanduser(self.conf.iproc.CODEDIR), 'runscript', 'anat_from_bids.py')
            cmd = [
                script,
                '--input', bids_anat_file,
                '--raw-output', dest_nii,
                '--reorient-output', dest_reorient_nii,
                '--work-dir', work_dirname
            ]
            logger.info(sp.list2cmdline(cmd))
            # scans.set_anat sets the scan object current scan data to the anat scan
            # e.g. scan.scan_name = 'NAT'
            logger.debug('setting sessionid')
            self.scans.set_anat(sessionid)
            logger.debug('set {}'.format(sessionid))
            logfile_base = self._io_file_fmt(cmd)
            job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def func_from_bids(self,overwrite=True):
        logger.debug('func_from_bids')
        job_spec_list = list()
        self.reset_steplog()
        for sessionid,sess in self.scans.sessions():
            sub = self.conf.iproc.SUB
            ses = sessionid
            for task_name,bold_scan in self.scans.tasks():
                task = self.scans.task_dict[task_name]
                bids_task_name,_ = split_task(task_name)
                run = bold_scan['BIDS_ID']
                if not run:
                    logger.debug('task column %s is set to zero in %s', task_name, self.conf.csv.SCANLIST)
                    continue
                logger.info('processing sub=%s, ses=%s, task=%s, run=%s', sub, ses, task_name, run)
                basename = 'ses-{ses}/func/sub-{sub}_ses-{ses}_task-{task}_run-{run}_bold.nii.gz'
                basename = basename.format(sub=sanitize(sub),
                                           ses=sanitize(ses),
                                           task=bids_task_name,
                                           run=run)
                bids_func_file = os.path.join(self.args.bids, basename)
                run_zpad = '{0:03d}'.format(int(bold_scan['BLD']))
                task_dirname = os.path.join(self.conf.iproc.NATDIR, ses, task_name)
                task_basename = '{0}_bld{1}_reorient_skip.nii.gz'.format(ses, run_zpad)
                sec_basename = '{0}_bld{1}'.format(ses, run_zpad)
                dest_nii = os.path.join(task_dirname, task_basename)
                sec_base = os.path.join(task_dirname, sec_basename)

                outfiles = [dest_nii,sec_base+'_echoTime.sec',sec_base+'_dwellTime.sec']
                if self._outfiles_skip(overwrite,outfiles):
                    continue
                elif not self.args.no_remove_files:
                    for outfile in outfiles:
                        os.remove(outfile) 
                        logging.debug('removed {}'.format(outfile))
                work_dirname = os.path.join(self.conf.iproc.WORKDIR, '{0}_{1}'.format(task_name, ses))
                for d in [work_dirname, task_dirname]:
                    if not os.path.exists(d):
                        os.makedirs(d)
                script = os.path.join(os.path.expanduser(self.conf.iproc.CODEDIR), 'runscript', 'func_from_bids.py')
                cmd = [
                    script,
                    '--input', bids_func_file,
                    '--output', dest_nii,
                    '--sec_base', sec_base,
                    '--skip', task['SKIP'],
                    '--num-vol', task['NUMVOL'],
                    '--work-dir', work_dirname
                ]
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def fmap_from_bids(self, overwrite=True):
        job_spec_list = list()
        self.reset_steplog()
        sub = self.conf.iproc.SUB
        preptool = self.conf.fmap.preptool
        b02b0_fname = os.path.join(self.conf.iproc.CODEDIR,'configs/b02b0.cnf')
        for sessionid,sess in self.scans.sessions():
            ses = sessionid
            for fmap_dir,fmap_scans in self.scans.fieldmaps():
                fmap_dirname = os.path.join(self.conf.iproc.NATDIR,sessionid,fmap_dir)
                fmap1_run = fmap_scans['FIRST_FMAP']
                fmap1_no_pad = "%03d" % int(fmap1_run)
                dest_fieldmap_nii = '{fdir}/{sessionid}_{fmap1_no}_fieldmap.nii.gz'.format(fdir=fmap_dirname,sessionid=sessionid,fmap1_no=fmap1_no_pad)
                outfiles = [dest_fieldmap_nii]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
                # find the files we need in BIDS directory

                if preptool == 'topup':
                    input1_bids_fname = fmap_scans['FIRST_BIDS_FNAME']
                    input2_bids_fname = fmap_scans['SECOND_BIDS_FNAME']
                    input1_json_fname = input1_bids_fname.rstrip('.nii.gz') + '.json'
                    input2_json_fname = input2_bids_fname.rstrip('.nii.gz') + '.json'

                    # final output directory and filenames
                    dest_fmap1_nii = os.path.join(fmap_dirname, 'fmap1_img.nii.gz')
                    dest_fmap2_nii = os.path.join(fmap_dirname, 'fmap2_img.nii.gz')
                    totalReadoutTime1 = commons.get_json_entity(input1_json_fname)
                    totalReadoutTime2 = commons.get_json_entity(input1_json_fname)
                    if totalReadoutTime1 != totalReadoutTime2:
                        raise ValueError('{} != {}. TotalReadoutTime from {} does not match {}'.format(totalReadoutTime1,totalReadoutTime2,input1_json_fname,input2_json_fname))
                    
                    script = os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'fmap_from_bids_topup.sh')
                    cmd = [script,input1_json_fname,input2_json_fname,fmap_dirname,self.conf.iproc.CODEDIR,totalReadoutTime2,dest_fieldmap_nii]
                
                elif preptool == 'fsl_prepare_fieldmap':
                    script = os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'fmap_from_bids.py')
                    dest_fmapm_nii = os.path.join(fmap_dirname, 'mag_img.nii.gz')
                    dest_fmapp_nii = os.path.join(fmap_dirname, 'pha_img.nii.gz')
                    bids_fmapp_file = fmap_scans['SECOND_BIDS_FNAME']
                    bids_fmapm_files = fmap_scans['FIRST_BIDS_FNAME']
                    cmd = [script]
                    cmd.append('--input-fmapm')
                    cmd.extend(bids_fmapm_files)
                    cmd.append('--input-fmapp')
                    cmd.append(bids_fmapp_file)
                    cmd.extend([
                        '--output-fmapm', dest_fmapm_nii,
                        '--output-fmapp', dest_fmapp_nii,
                        '--output-fieldmap', dest_fieldmap_nii,
                        '--work-dir', os.path.join(self.conf.iproc.WORKDIR, '{0}_{1}'.format('FMAP', ses))
                        ])
                    logger.info('fmapp file is %s', bids_fmapp_file)

                else:
                    raise Exception('unknown preptool {}'.format(preptool))
                # create output directory
                if not os.path.exists(fmap_dirname):
                    os.makedirs(fmap_dirname)

                logger.info(sp.list2cmdline(cmd))
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def xnat_to_nii_gz_anat(self, overwrite=True):
        stepname_base = 'xnat_to_nii_gz_anat'
        logger.debug(stepname_base)

        job_spec_list = []
        self.reset_steplog()
        for sessionid,sess in self.scans.anat_sessions():
            ANAT_SCAN_NO = sess.anat_no
            anat_no_padded = '%03d' % int(ANAT_SCAN_NO)
            anat_dir = os.path.join(self.conf.iproc.NATDIR, sessionid, 'ANAT')
            anat_name = sessionid + '_mpr' + anat_no_padded
            work_name = 'ANAT_' + sessionid
            workdir_anat = os.path.join(self.conf.iproc.WORKDIR, work_name) ## why workdir? Shouldn't this be outdir?
            outnii = os.path.join(anat_dir,anat_name+'.nii.gz')
            outnii_reorient = os.path.join(anat_dir,anat_name+'_reorient.nii.gz')
            outfiles = [outnii,outnii_reorient]
            if self._outfiles_skip(overwrite,outfiles):
                continue

            logger.debug('making {}'.format(anat_dir))
            if not os.path.exists(anat_dir):
                os.makedirs(anat_dir)

            codedir = os.path.expanduser(self.conf.iproc.CODEDIR)
            cmd = [os.path.join(codedir, 'runscript', 'xnat_to_nii_gz_anat.sh'),
                workdir_anat,
                sessionid,
                ANAT_SCAN_NO,
                self.conf.iproc.CODEDIR,
                self.conf.xnat.XNAT_ALIAS,
                self.conf.xnat.XNAT_PROJECT,
                outnii,
                outnii_reorient
                ]
            # scans.set_anat sets the scan object current scan data to the anat scan
            # e.g. scan.scan_name = 'NAT'
            logger.debug('setting sessionid')
            self.scans.set_anat(sessionid)
            logger.debug('set {}'.format(sessionid))
            logfile_base = self._io_file_fmt(cmd)
            job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def recon_all(self, overwrite=True):
        logger.debug('fs_recon_all')
        job_spec_list = []
        self.reset_steplog()
        for sessionid,sess in self.scans.anat_sessions():
            # Zero-pad ANAT scan number
            anatno = "%03d" % int(sess.anat_no)
            anat_vol = os.path.join(self.conf.fs.SUBJECTS_DIR,sessionid,'mri','T1.mgz')
            pial_surf = os.path.join(self.conf.fs.SUBJECTS_DIR,sessionid,'surf','lh.pial')
            mpr_reorient = os.path.join(self.conf.iproc.NATDIR,sessionid,'ANAT','{SESS}_mpr{ANATNO}_reorient.nii.gz'.format(SESS=sessionid,ANATNO=anatno))
            outfiles = [anat_vol,pial_surf]
            if self._outfiles_skip(overwrite,outfiles):
                continue

            cmd = [os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'recon_all.sh'),
                self.conf.iproc.SUB,
                sessionid,
                mpr_reorient,
                self.conf.fs.SUBJECTS_DIR,
                self.conf.out_atlas.FS6,
                self.conf.iproc.SCRATCHDIR,
                self.conf.iproc.CODEDIR]

            self.scans.set_anat(sessionid)
            logfile_base = self._io_file_fmt(cmd)
            job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def xnat_to_nii_gz_task(self, overwrite=True):
        logger.debug('xnat_to_nii_gz_task')

        job_spec_list = []
        self.reset_steplog()
        # scans.sessions() and scans.tasks() are special iterators that will
        # update the state of the scan object,
        # for use by _io_file_fmt or other functions.
        # This way the scans object always has a readily-accessible description
        #of the scan it is currently operating on.
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                task_scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(task_scan_no)
                file_dir = os.path.join(self.conf.iproc.NATDIR, sessionid, task_type)
                file_name = sessionid+"_bld"+bold_no+"_reorient_skip.nii.gz"
                outnii=os.path.join(file_dir,file_name)
                outfiles = [outnii]
                if self._outfiles_skip(overwrite,outfiles):
                    continue

                work_name = task_type+"_"+sessionid
                workdir = os.path.join(self.conf.iproc.WORKDIR, work_name)
                task = self.scans.task_dict[task_type]
                SKIP = task["SKIP"]
                NUMVOL = task["NUMVOL"]
                
                if not os.path.exists(workdir):
                    os.makedirs(workdir)

                if not os.path.exists(file_dir):
                    os.makedirs(file_dir)

                cmd=[os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'xnat_to_nii_gz_task.sh'),
                        workdir,
                        sessionid,
                        task_scan_no,
                        self.conf.iproc.CODEDIR,
                        self.conf.xnat.XNAT_ALIAS,
                        self.conf.xnat.XNAT_PROJECT,
                        outnii,
                        SKIP,
                        NUMVOL]

                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()

        return job_spec_list

    def xnat_to_nii_gz_fieldmap(self, overwrite=True):
        '''prepares fieldmap dicoms for use in fm_unw.sh. Can handle both double echo and reverse plane encode fieldmaps'''
        job_spec_list = []
        self.reset_steplog()
        preptool = self.conf.fmap.preptool
        if preptool == 'topup':
            script = os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'xnat_to_nii_gz_fm_topup.sh')
        elif preptool == 'fsl_prepare_fieldmap':
            script = os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'xnat_to_nii_gz_fm.sh')
        else:
            raise Exception('unknown preptool {}'.format(preptool))
        for sessionid,sess in self.scans.sessions():
            for fmap_dir,fmap_scans in self.scans.fieldmaps():
                file_dir = os.path.join(self.conf.iproc.NATDIR,sessionid,fmap_dir)
                # phase scan is usually right after FMAP_MAG(m for magnitude).
                # this is the second, smaller set of field map dicoms in xnat
                # Phase scan has half the number of slices as Mag.
                logger.debug(fmap_scans)
                fmap1_no = str(fmap_scans['FIRST_FMAP'])
                fmap2_no = str(fmap_scans['SECOND_FMAP'])
                if fmap1_no == '0' or fmap2_no == '0':
                    raise ValueError('something went wrong with fieldmap type assignment. \n {},{} \n {}'.format(fmap1_no,fmap2_no,fmap_scans))
                fmap1_no_pad = "%03d" % int(fmap1_no)
                outfile = '{fdir}/{sessionid}_{fmap1_no}_fieldmap.nii.gz'.format(fdir=file_dir,sessionid=sessionid,fmap1_no=fmap1_no_pad)
                outfiles = [outfile]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
                #Create Output Directory
                if not os.path.exists(file_dir):
                    os.makedirs(file_dir)
                

                cmd = [os.path.join(self.conf.iproc.CODEDIR,'modwrap.sh'), 'module load fsl/4.0.3-ncf', 'module load fsl/5.0.4-ncf',
                            script,
                                    sessionid,
                                    fmap1_no,
                                    fmap2_no,
                                    file_dir,
                                    self.conf.iproc.CODEDIR,
                                    self.conf.xnat.XNAT_ALIAS,
                                    self.conf.xnat.XNAT_PROJECT,
                                    outfile]

                # todo: generalize directories

                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list

    def sesst_prep(self, overwrite=True):
        logger.debug('sesst_prep')
        job_spec_list = []
        self.reset_steplog()
        mpr_reorient = os.path.join(self.conf.template.TEMPLATE_DIR,'{}_mpr_reorient.nii.gz'.format(self.conf.T1.T1_SESS))
        outfiles = [mpr_reorient]
        if self._outfiles_skip(overwrite,outfiles):
            return [] 
        
        run_cmd = [os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'sesst_prep.sh'),
            self.conf.fs.SUBJECTS_DIR,
            self.conf.T1.T1_SESS,
            self.conf.template.TEMPLATE_DIR,
            mpr_reorient]
        
        if not self.conf.T1.T1_SESS:
            raise Exception('no T1.T1_SESS in conf file')

        self.scans.set_anat(self.conf.T1.T1_SESS)
        logfile_base = self._io_file_fmt(run_cmd)
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
       
    def fslroi_reorient_skip(self, overwrite=True):
        logger.debug('fslroi_reorient_skip') 
                           
        self.reset_steplog()
        outfile_fname='{SUB}_D01_{BNAME}_bld{BLD}_midvol.nii.gz'.format(SUB=self.conf.iproc.SUB,BNAME=self.conf.template.MIDVOL_BOLDNAME,BLD=self.conf.template.MIDVOL_BOLDNO)
        fslroi_outfile = os.path.join(self.conf.template.TEMPLATE_DIR,outfile_fname)
        outfiles = [fslroi_outfile]
        if self._outfiles_skip(overwrite,outfiles):
            return []

        self._set_rmfiles('create_upsampled_midvol_target',outfile_fname) 

        fslroi_infile = os.path.join(self.conf.iproc.NATDIR,self.conf.template.MIDVOL_SESS,self.conf.template.MIDVOL_BOLDNAME,
            self.conf.template.MIDVOL_SESS+'_bld'+self.conf.template.MIDVOL_BOLDNO+'_reorient_skip.nii.gz')
    
        run_cmd=[os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'fslroi_reorient_skip.sh'),
            fslroi_infile, 
            fslroi_outfile,
            self.conf.template.MIDVOL_VOLNO,
            '1']
        try:
            self.scans.set_midvol(self.conf)
        except KeyError as e:   
            logger.error('remember to set variables in [template] section of config!')
            raise e
        logfile_base = self._io_file_fmt(run_cmd)
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def fm_unwarp_midvol(self, overwrite=True):
        stepname ='fm_unwarp_midvol'
        logger.debug(stepname)
        
        self.reset_steplog()

        midvol_sessid = self.conf.template.MIDVOL_SESS
        bold_name = self.conf.template.MIDVOL_BOLDNAME
        midvol_sess = self.scans.scan_by_session[midvol_sessid]
        stripped_midvol_num = int(self.conf.template.MIDVOL_BOLDNO)
        midvol_scan = midvol_sess.bold_scans[stripped_midvol_num]
        fm_tasktype = midvol_scan['FMAP_DIR']
        
        fm_bold_no = "%03d" % int(midvol_scan['FIRST_FMAP'])
        fdir = os.path.join(self.conf.iproc.NATDIR,midvol_sessid,fm_tasktype)

        img = os.path.join(self.conf.template.TEMPLATE_DIR,'{SUB}_D01_{BNAME}_bld{BLD}_midvol.nii.gz'.format(SUB=self.conf.iproc.SUB,BNAME=bold_name,BLD=self.conf.template.MIDVOL_BOLDNO))
        dest_dir = os.path.dirname(img)
        warp_dir = dest_dir + '/fm_unwarp{}'.format(self.conf.template.MIDVOL_BOLDNO)
        unwarped_img = os.path.join(self.conf.template.TEMPLATE_DIR,'{SUB}_D01_{BNAME}_midvol_unwarp.nii.gz'.format(SUB=self.conf.iproc.SUB,BNAME=bold_name))
        # TODO: finish this, make naming align to conventions.
        outfiles = [unwarped_img]
        if self._outfiles_skip(overwrite,outfiles):
            return []

        fsl_unwarp_direction = self._unwarp_direction_from_sidecar(self.conf.template.TEMPLATE_DIR,midvol_sessid,self.conf.template.MIDVOL_BOLDNO)
        rmfiles = self._get_rmfiles(stepname)
        self._set_rmfiles('create_upsampled_midvol_target',unwarped_img) 

        run_cmd = [os.path.join(self.conf.iproc.CODEDIR,'modwrap.sh'), 'module load fsl/4.0.3-ncf', 'module load fsl/5.0.4-ncf',
                    os.path.join(self.conf.iproc.CODEDIR, 'runscript', 'fm_unw.sh'),
                    midvol_sessid,
                    fdir,
                    img,
                    unwarped_img,
                    fm_bold_no,
                    dest_dir,
                    warp_dir,
                    fsl_unwarp_direction]
        self.scans.set_midvol(self.conf)
        logfile_base = self._io_file_fmt(run_cmd)

        if not self.args.no_remove_files:
            run_cmd.append(" ".join(rmfiles))

        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles,rmfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def create_upsamped_midvol_target(self, overwrite=True):
        stepname='create_upsamped_midvol_target'
        logger.debug(stepname) 
        
        self.reset_steplog()
        outfile = '{TEMPLATE_DIR}/{SUB}_D01_{BNAME}_midvol_unwarp_1p2i.nii.gz'.format(TEMPLATE_DIR=self.conf.template.TEMPLATE_DIR,SUB=self.conf.iproc.SUB,BNAME=self.conf.template.MIDVOL_BOLDNAME)
        midvol_hdr = '{TEMPLATE_DIR}/{SUB}_D01_{BNAME}_midvol_hdr_tmp.nii.gz'.format(TEMPLATE_DIR=self.conf.template.TEMPLATE_DIR,SUB=self.conf.iproc.SUB,BNAME=self.conf.template.MIDVOL_BOLDNAME)
        outfiles = [outfile]
        if self._outfiles_skip(overwrite,outfiles):
            return []
    
        rmfiles = self._get_rmfiles(stepname)
        rmfiles.append(midvol_hdr)

        run_cmd = [os.path.join(self.conf.iproc.CODEDIR,'runscript','create_upsampled_midvol_target.sh'),
                    self.conf.iproc.SUB,
                    self.conf.template.MIDVOL_BOLDNAME,
                    self.conf.template.TEMPLATE_DIR,
                    outfile]
    
        self.scans.set_midvol(self.conf)
        logfile_base = self._io_file_fmt(run_cmd)
        if not self.args.no_remove_files:
            run_cmd.append(" ".join(rmfiles))
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles,rmfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def fm_unwarp_and_mc_to_midvol(self, overwrite=True):
        # formerly p2
        # unwarps scans according to fieldmap specified in boldscan csv
        # aligns all timepoints to midvoltarg, product of create_upsamped_midvol_target
        # also performs motion correction relative to midvoltarg
        stepname = 'fm_unwarp_and_mc_to_midvol'
        logger.debug(stepname) 
         
        job_spec_list = []
        self.reset_steplog()
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                fm_bold_no = "%03d" % int(bold_scan['FIRST_FMAP'])
                numvol=self.scans.task_dict[task_type]['NUMVOL']

                fm_task_type = bold_scan['FMAP_DIR']

                fdir = os.path.join(self.conf.iproc.NATDIR,sessionid,fm_task_type)
                outputdir = os.path.join(self.conf.iproc.NATDIR,sessionid,task_type)
    
                mc_in = os.path.join(outputdir,"%s_bld%s_reorient_skip.nii.gz" % (sessionid,bold_no))
                mc_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc" % (sessionid,bold_no))
                subjid = self.conf.iproc.SUB
                target = os.path.join(self.conf.template.TEMPLATE_DIR,"%s_midvol_unwarp_1p2i" % subjid)
                flirt_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_midvol_to_midvoltarg.nii.gz" % (sessionid, bold_no))
                flirt_mat_out = os.path.join(outputdir,"%s_to_midvoltarg.mat" % task_type)
                scan_type="%s_bld%s" % (sessionid,bold_no)
                midvol=os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_midvol" % (sessionid,bold_no))
                regressors_mc_dat = os.path.join(outputdir,'{}_regressors_mc.dat'.format(scan_type))
                numvol_list = [str(x) for x in range(int(numvol))]
                mc_out_matdir = mc_out+'.mat'
                mc_mats = [os.path.join(mc_out_matdir,'MAT_{}'.format(n.zfill(4))) for n in numvol_list ]
                affix_list = ['abs','abs_mean','rel','rel_mean']
                mc_rms = ['{MC_OUT}_{AFFIX}.rms'.format(MC_OUT=mc_out,AFFIX=a) for a in affix_list]
                outfiles = [flirt_out,flirt_mat_out,regressors_mc_dat]
                outfiles.extend(mc_mats)
                dest_dir = os.path.dirname(midvol)
                warp_dir = dest_dir + '/fm_unwarp{}'.format(bold_no)
                EF_UD = os.path.join(warp_dir, 'EF_UD_warp.nii.gz')
                outfiles.append(EF_UD)
                if self._outfiles_skip(overwrite,outfiles):
                    continue
                # if reorient_skip_mc.mat dir exists, delete, so we don't keep 
                # creating endless reorient_skip_mc.mat+++++++ directories
                if os.path.isdir(mc_out_matdir):   
                    shutil.rmtree(mc_out_matdir)
    
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)

                fsl_unwarp_direction = self._unwarp_direction_from_sidecar(outputdir,sessionid,bold_no)

                self._set_rmfiles('combine_warps_parallel',mc_mats)
                rmfiles = self._get_rmfiles(stepname)
                #add intermediates created by mcflirt to deletion list for this step
                rmfiles.append(mc_out+'.nii.gz')
                rmfiles += mc_rms

                # note: p2.sh calls fm_unw.sh
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','fm_unwarp_and_mc_to_midvol.sh'),
                    mc_in,
                    mc_out,
                    target,
                    flirt_out,
                    flirt_mat_out,
                    scan_type,
                    numvol,
                    outputdir,
                    self.conf.iproc.CODEDIR,
                    sessionid,
                    fm_bold_no,
                    midvol,
                    fdir,
                    regressors_mc_dat,
                    dest_dir,
                    warp_dir,
                    fsl_unwarp_direction]
       
                logfile_base = self._io_file_fmt(cmd)
                if not self.args.no_remove_files:
                    cmd.append(" ".join(rmfiles))
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles,rmfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def fslmerge_meantime(self,merged_vol,globfiles,reorient=False, overwrite=True):
        logger.debug('fslmerge_meantime') 
        #wrapper on fslmerge
        # this is used multiple times in various locations
        #so I'm not going to worry about rmfiles right now
        outfiles = [merged_vol]
        self.reset_steplog()
        if self._outfiles_skip(overwrite,outfiles):
            return []
        fslmerge_cmd = [os.path.join(self.conf.iproc.CODEDIR,'runscript','fslmerge_meantime.sh')]
        fslmerge_cmd.append(merged_vol)
        run_cmd = fslmerge_cmd+globfiles
    
        self.scans.set_name('Merge')
        logfile_base = self._io_file_fmt(run_cmd)
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
  
    def align_to_midvol_mean(self,overwrite=True):
        logger.debug('align_to_midvol_mean') 
        # alignment to the midvols_mean template. 
        #as in calc_mc_and_align_to_midvol, we are aligning the 
        #field-map unwarped, upsampled middle volumes of each run to a target-- 
        #the midvol_mean (formerly meanBOLD) target is produced by a 
        #fslmerge_meantime over midvols from all sessions after alignment to 
        #the midvol target.
        # formerly p3.
        job_spec_list = []
        self.reset_steplog()
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
    
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
       
                outputdir = os.path.join(self.conf.iproc.NATDIR, sessionid, task_type)
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)
    
                target = self.conf.template.midvols_mean
                bet_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_midvol_unwarp" % (sessionid,bold_no))
    
                # was on_allscans
                flirt_out = os.path.join(outputdir,"%s_bld%s_on_midmean.nii.gz" % (sessionid, bold_no))
                flirt_mat_out = os.path.join(outputdir,"%s_to_allscans.mat" % task_type)
    
                outfiles = [flirt_out, flirt_mat_out]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','align_to_midvol_mean.sh'),
                    bet_out,
                    target,
                    flirt_out,
                    flirt_mat_out]
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list    
    
    def bbreg(self, overwrite=True):
        # this computes a registration from the midvols_mean to the sesst NAT T1.
        logger.debug('bbreg') 
        
        self.reset_steplog()
        # might want to change to reflect change in midvols_mean, which was 
        #{sub}_meanBOLD_allscans
        vreg = os.path.join(self.conf.template.TEMPLATE_DIR,'{}_allscans_meanBOLD_to_T1'.format(self.conf.iproc.SUB))
        vreg_dat = '{}.dat'.format(vreg)
        vreg_mat = '{}.mat'.format(vreg)
        outfiles = [vreg_dat, vreg_mat]
        affix_list = ['log','param','mincost','sum']
        bbreg_cruft = ['{REG}.{AFFIX}'.format(REG=vreg,AFFIX=a) for a in affix_list]
        movimg = self.conf.template.midvols_mean
        if self._outfiles_skip(overwrite,outfiles):
            return([]) 
        
        rmfiles = self._get_rmfiles('bbreg')
        rmfiles+=bbreg_cruft

        run_cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','bbreg.sh'),
            self.conf.T1.T1_SESS,
            self.conf.template.TEMPLATE_DIR,
            movimg,
            vreg_dat,
            vreg_mat]
    
        self.scans.set_anat(self.conf.T1.T1_SESS)
        logfile_base = self._io_file_fmt(run_cmd)
        if not self.args.no_remove_files:
            run_cmd.append(" ".join(rmfiles))
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles,rmfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    # past this point we're working with output spaces
    def compute_T1_MNI_warp(self, overwrite=True):
        logger.debug('compute_T1_MNI_warp') 
        
        self.reset_steplog()
        # I know this looks like some kind of error but that's its name
        invwarp_out = '{TARGDIR}/MNI_to_{SESST}_mni_underlay.mat.nii.gz'.format(TARGDIR=self.conf.template.TEMPLATE_DIR,SESST=self.conf.T1.T1_SESS)
        outfiles = [invwarp_out]
        if self._outfiles_skip(overwrite,outfiles):
            return([])
        
        run_cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','compute_T1_MNI_warp.sh'),
            self.conf.template.TEMPLATE_DIR,
            invwarp_out,
            self.conf.out_atlas.MNI111,
            self.conf.out_atlas.MNI111_BRAIN,
            self.conf.out_atlas.MNI111_BRAINMASK]
    
        self.scans.set_anat(self.conf.T1.T1_SESS)
        logfile_base = self._io_file_fmt(run_cmd)
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def reg_MNI_CSF_WM_to_T1(self, overwrite=True):
        logger.debug('reg_MNI_CSF_WM_to_T1') 
        
        self.reset_steplog()
        csf_out = os.path.join(self.conf.template.TEMPLATE_DIR,'mni_masks/csf_mask_mpr_reorient.nii.gz')
        wm_out = os.path.join(self.conf.template.TEMPLATE_DIR,'mni_masks/wm_mask_mpr_reorient.nii.gz')
        outfiles = [csf_out,wm_out]
        if self._outfiles_skip(overwrite,outfiles):
            return([])
        
        run_cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','reg_MNI_CSF_WM_to_T1.sh'),
            self.conf.template.TEMPLATE_DIR,
            self.conf.T1.T1_SESS,
            csf_out,
            wm_out,
            self.conf.iproc.MASKSDIR,
            self.conf.out_atlas.MNI111]
    
        self.scans.set_anat(self.conf.T1.T1_SESS)
        logfile_base = self._io_file_fmt(run_cmd)
        job_spec_list = [JobSpec(run_cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def size_brainmask(self, overwrite=True):
        logger.debug('size_brainmask') 
        
        self.reset_steplog()
        mni_underlay_dilated_bm_out = '{}/anat_mni_underlay_brain_mask_dil10.nii.gz'.format(self.conf.template.TEMPLATE_DIR)
        mpr_bm_out = '{}/mpr_reorient_brain_mask_dil10.nii.gz'.format(self.conf.template.TEMPLATE_DIR)
        outfiles = [mni_underlay_dilated_bm_out,mpr_bm_out]
        if self._outfiles_skip(overwrite,outfiles):
            return([]) 
    
        cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','size_brainmask.sh'),
            self.conf.template.TEMPLATE_DIR,
            mni_underlay_dilated_bm_out]
    
        self.scans.set_anat(self.conf.T1.T1_SESS)
        logfile_base = self._io_file_fmt(cmd)
        job_spec_list = [JobSpec(cmd,logfile_base,outfiles)]
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    def combine_warps_parallel(self,anat_space, overwrite=True):
        # this combines and applies warps across all slices of the BOLD data,
        # moving it, one time point at a time, into an output volume space.
        # The individual-time-point volumes are not moved into the corresponding
        # out volume directory until the next step, as they are intermediates.
        logger.debug('combine_warps_parallel') 
    
        job_spec_list = []
        self.reset_steplog()
        # 4 scan sessions per subject in DN2
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                numvol=self.scans.task_dict[task_type]['NUMVOL']

                
                outputdir = os.path.join(self.conf.iproc.NATDIR, sessionid, task_type)
                pyscript = os.path.join(self.conf.iproc.CODEDIR,'iProc_p4_sbatch_combined.py')
                if anat_space=='T1':
                    outfile_base = '{}_TARG_FILE'.format(anat_space)
                elif anat_space=='MNI': 
                    outfile_base = '{}_TARG_FILE'.format(anat_space)
                else:
                    raise NotImplementedError('anat_space parameter to combine_warps_parallel() must be T1 or MNI')
                mat_dir = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc.mat" % (sessionid,bold_no))
                volnums = [str(n) for n in range(int(numvol))]
                out_file_names = ['{}_{}.nii.gz'.format(outfile_base,n.zfill(4)) for n in volnums] 
                bld_dir = '{}_{}'.format(bold_no,anat_space)
                outfiles = [os.path.join(outputdir,bld_dir, f) for f in out_file_names]
    
                split_in= os.path.join(outputdir,"%s_bld%s_reorient_skip.nii.gz" % (sessionid,bold_no))
                rmfiles = self._get_rmfiles('combine_warps_parallel')
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript/combine_warps_parallel.sbatch'),
                split_in,
                pyscript,
                mat_dir,
                numvol,
                outputdir,
                subjid,
                task_type,
                sessionid,
                bold_no,
                self.conf.template.TEMPLATE_DIR,
                anat_space,
                self.conf.out_atlas.MNI111,
                self.conf.iproc.SCRATCHDIR]
    
                logfile_base = self._io_file_fmt(cmd)

                if not self.args.no_remove_files:
                    cmd.append(" ".join(rmfiles))
                job_spec = JobSpec(cmd,logfile_base,outfiles,rmfiles)
                if self._outfiles_skip(overwrite,outfiles):
                    # this will allow the executor to avoid running the job, 
                    # while still having a dummy job object to pin dependent jobs to 
                    job_spec.skip = True

                job_spec_list.append(job_spec)
        self.scans.reset_default_sessionid()
        return job_spec_list    
    
    def combine_warps_post(self, overwrite=True):
        # this combines the individual-time-point volumes created by 
        #combine_warps_parallel, and places the resulting volume in the
        # output volume space directory for NAT111
        logger.debug('combine_warps_post') 

        job_spec_list = []

        self.reset_steplog()
        dilated_brainmask = os.path.join(self.conf.template.TEMPLATE_DIR,'mpr_reorient_brain_mask_dil10.nii.gz')
        if not os.path.exists(dilated_brainmask):
            logger.error(dilated_brainmask)
            logger.error('brainmask does not exist!')
            #TODO: more graceful way of exiting
            exit(1)
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                rmfiles = self._get_rmfiles('combine_warps_post')
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                numvol=self.scans.task_dict[task_type]['NUMVOL']
                outputdir = os.path.join(self.conf.iproc.NAT111DIR, sessionid, task_type)
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir) 
                indir = os.path.join(self.conf.iproc.NATDIR, sessionid, task_type)
                bld_dir = '{}_T1'.format(bold_no)
                targ_warp_files = os.path.join(indir,bld_dir, "T1_TARG_WARP_")
                merge_in = os.path.join(indir,bld_dir,"T1_TARG_FILE_")
                targfile_glob = merge_in + '*' 
                targwarp_glob = targ_warp_files + '*'
                rmfiles += [targwarp_glob,targfile_glob]
                merge_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_tmp.nii.gz" % (sessionid,bold_no))
                rmfiles.append(merge_out)
                reorient_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat.nii.gz" % (sessionid,bold_no))
                dilmask = os.path.join(self.conf.template.TEMPLATE_DIR,"mpr_reorient_brain_mask_dil10.nii.gz")
                mean_out = os.path.join(outputdir, "%s_bld%s_reorient_skip_mc_unwarp_anat_mean.nii.gz" % (sessionid,bold_no))
                # do not delete mean_out -- needed for QC
                midvol_num = str(int(numvol)/2)
                midvol_pad = midvol_num.zfill(3)
                midvol_out = os.path.join(outputdir, "%s_bld%s_reorient_skip_mc_unwarp_anat_vol%s.nii.gz" % (sessionid,bold_no,midvol_pad))
                outfiles = [midvol_out,reorient_out,mean_out]

                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript/combine_warps_post.sh'),
                merge_in,
                merge_out,
                numvol,
                reorient_out,
                dilmask,
                mean_out,
                midvol_out]

                logfile_base = self._io_file_fmt(cmd)

                if not self.args.no_remove_files:
                    cmd.append(" ".join(rmfiles))
                job_spec = JobSpec(cmd,logfile_base,outfiles,rmfiles)
                if self._outfiles_skip(overwrite,outfiles):
                    # this will allow the executor to avoid running the job, 
                    # while still having a dummy job object to pin dependent jobs to 
                    job_spec.skip = True

                job_spec_list.append(job_spec)
 
        self.scans.reset_default_sessionid()
        return job_spec_list    

    def combine_warps_post_MNI(self, overwrite=True):
        # this combines the individual-time-point volumes created by 
        #combine_warps_parallel, and places the resulting volume in the
        # output volume space directory for MNI111
        logger.debug('combine_warps_post_MNI') 
 
        job_spec_list = []
        self.reset_steplog()
        # 4 scan sessions per subject in DN2
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                rmfiles = self._get_rmfiles('combine_warps_parallel_MNI')
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                numvol=self.scans.task_dict[task_type]['NUMVOL']

                outputdir = os.path.join(self.conf.iproc.MNI111DIR, sessionid, task_type)
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir)
                indir = os.path.join(self.conf.iproc.NATDIR, sessionid, task_type)
                bld_dir = '{}_MNI'.format(bold_no)
                merge_in = os.path.join(indir,bld_dir,"MNI_TARG_FILE_")
                targ_warp_files = os.path.join(indir,bld_dir, "MNI_TARG_WARP_")
                targfile_glob = merge_in + '*' 
                targwarp_glob = targ_warp_files + '*'
                rmfiles += [targwarp_glob,targfile_glob]
                dilmask = os.path.join(self.conf.template.TEMPLATE_DIR,"anat_mni_underlay_brain_mask_dil10.nii.gz")
                midvol_num = str(int(numvol)/2)
                midvol_pad = midvol_num.zfill(3)
                midvol_out = os.path.join(outputdir, "%s_bld%s_reorient_skip_mc_unwarp_anat_mni_vol%s.nii.gz" % (sessionid,bold_no,midvol_pad))
    
                mean_out = os.path.join(outputdir, "%s_bld%s_reorient_skip_mc_unwarp_anat_mni_mean.nii.gz" % (sessionid,bold_no))
                merge_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_mni.nii.gz" % (sessionid,bold_no))
                # we will not delete merge_out because it is the output
                outfiles = [midvol_out,mean_out,merge_out]
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript/combine_warps_post_MNI.sh'),
                    merge_in,
                    merge_out,
                    numvol,
                    dilmask,
                    mean_out,
                    midvol_out]
    
                logfile_base = self._io_file_fmt(cmd)

                if not self.args.no_remove_files:
                    cmd.append(" ".join(rmfiles))
                job_spec = JobSpec(cmd,logfile_base,outfiles,rmfiles)
                if self._outfiles_skip(overwrite,outfiles):
                    # this will allow the executor to avoid running the job, 
                    # while still having a dummy job object to pin dependent jobs to 
                    job_spec.skip = True

                job_spec_list.append(job_spec)
        self.scans.reset_default_sessionid()
        return job_spec_list    
    
    def calculate_nuisance_params(self,overwrite=True):
        logger.debug('calculate_nuisance_params') 
    
        self.reset_steplog()
        job_spec_list = []
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                outputdir = os.path.join(self.conf.iproc.NAT111DIR,  sessionid, task_type)
                natdir = os.path.join(self.conf.iproc.NATDIR,  sessionid, task_type)
                resid_in = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat.nii.gz" % (sessionid,bold_no))
                csf_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_csf_ts.dat" % (sessionid,bold_no))
                csf_mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mni_masks","csf_mask_mpr_reorient.nii.gz")
                wm_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_wm_ts.dat" % (sessionid,bold_no))
                wm_mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mni_masks","wm_mask_mpr_reorient.nii.gz")
                wb_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_wb_ts.dat" % (sessionid,bold_no))
                wb_mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mni_masks","wb_mask_mpr_reorient.nii.gz")
                phys_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_phys_ts.dat" % (sessionid,bold_no))
                mc_ts = os.path.join(natdir,"%s_bld%s_reorient_skip_mc.par" % (sessionid,bold_no))
                nuis_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_nuis_ts.dat" % (sessionid,bold_no))
                scang="%s_bld%s" % (sessionid,bold_no)
                nuis_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_nuis.dat" % (sessionid,bold_no))
    
                outfiles = [nuis_out]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','calculate_nuisance_params.sh'),
                    resid_in,
                    csf_ts,
                    csf_mask,
                    wm_ts,
                    wm_mask,
                    wb_ts,
                    wb_mask,
                    phys_ts,
                    mc_ts,
                    nuis_ts,
                    scang,
                    nuis_out,
                    outputdir]
    
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list 
    
    def calculate_wholebrain_only(self, overwrite=True):
        pass

    def nuisance_regress(self,anat_space, overwrite=True):
        logger.debug('nuisance_regress') 
    
        self.reset_steplog()
        job_spec_list = []
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)

                nat111dir = os.path.join(self.conf.iproc.NAT111DIR, sessionid, task_type)
                nuis_out = os.path.join(nat111dir,"%s_bld%s_reorient_skip_mc_unwarp_anat_nuis.dat" % (sessionid,bold_no))
                outputdir = None
                if anat_space == 'MNI111': 
                    outputdir = os.path.join(self.conf.iproc.MNI111DIR, sessionid, task_type)
                    resid_in = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_mni.nii.gz" % (sessionid,bold_no))
                    resid_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_mni_resid" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"anat_mni_underlay_brain_mask.nii.gz")
                elif anat_space == 'NAT111': 
                    outputdir = nat111dir
                    resid_in = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat.nii.gz" % (sessionid,bold_no))
                    resid_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_resid" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mpr_reorient_brain_mask.nii.gz")
                else:
                    raise NotImplementedError('anat_space parameter to nuisance_regress() must be T1 or MNI')
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir) 
                fullpath_resid_out = os.path.join(outputdir,resid_out)
                resid_outs = [f.format(fullpath_resid_out) for f in ['{}+tlrc.HEAD','{}+tlrc.BRIK']]
                outfiles = resid_outs + [nuis_out]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','nuisance_regress.sbatch'),
                    resid_in,
                    nuis_out,
                    resid_out,
                    outputdir,
                    mask,
                    self.conf.iproc.CODEDIR,
                    self.conf.iproc.SCRATCHDIR]
    
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list 

    def wholebrain_only_regress(self,anat_space, overwrite=True):
        # this includes steps from nuisance variable calculation,
        #regression, and bandpassing, all in one step.
        # this is becasue we're only regressing out the whole-brain signal,
        #which is much simpler.
        logger.debug('wholebrain_only_regress') 
    
        self.reset_steplog()
        job_spec_list = []
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)

                nat111dir = os.path.join(self.conf.iproc.NAT111DIR, sessionid, task_type)
                if anat_space == 'MNI111': 
                    outputdir = os.path.join(self.conf.iproc.MNI111DIR, sessionid, task_type)
                    resid_in = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_mni.nii.gz" % (sessionid,bold_no))
                    wb_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_wb_ts.dat" % (sessionid,bold_no))
                    wb_mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mni_masks","wm_mask_1mm.nii.gz")
                    resid_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_mni_wb_resid" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"anat_mni_underlay_brain_mask.nii.gz")
                elif anat_space == 'NAT111': 
                    outputdir = nat111dir
                    resid_in = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat.nii.gz" % (sessionid,bold_no))
                    wb_ts = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_mni_wb_ts.dat" % (sessionid,bold_no))
                    wb_mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mni_masks","wb_mask_mpr_reorient.nii.gz")
                    resid_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_wb_resid" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mpr_reorient_brain_mask.nii.gz")
                else:
                    raise NotImplementedError('anat_space parameter to nuisance_regress() must be T1 or MNI')
 
                fullpath_resid_out = os.path.join(outputdir,resid_out+'.nii.gz')
                
                outfiles = [fullpath_resid_out]
                if self._outfiles_skip(overwrite,outfiles):
                    continue

                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','wholebrain_only_regress.sh'),
                    resid_in,
                    wb_ts,
                    wb_mask,
                    resid_out,
                    outputdir,
                    mask,
                    self.conf.iproc.CODEDIR,
                    self.conf.iproc.SCRATCHDIR]
                   
                    
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list 
     
    def bandpass(self,anat_space, overwrite=True):
        logger.debug('bandpass') 
    
        job_spec_list = []
        self.reset_steplog()
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                if anat_space == 'MNI111':
                    outputdir = os.path.join(self.conf.iproc.MNI111DIR, sessionid, task_type)
                    resid_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_mni_resid+tlrc" % (sessionid,bold_no))
                    bpss_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_mni_resid_bpss" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"anat_mni_underlay_brain_mask.nii.gz")
                elif anat_space == 'NAT111':
                    outputdir = os.path.join(self.conf.iproc.NAT111DIR, sessionid, task_type)
                    resid_out = os.path.join(outputdir,"%s_bld%s_reorient_skip_mc_unwarp_anat_resid+orig" % (sessionid,bold_no))
                    bpss_out = "%s_bld%s_reorient_skip_mc_unwarp_anat_resid_bpss" % (sessionid,bold_no)
                    mask = os.path.join(self.conf.template.TEMPLATE_DIR,"mpr_reorient_brain_mask.nii.gz")
                else:
                    raise NotImplementedError('anat_space parameter to bandpass() must be T1 or MNI')
                fullpath_bpss_out_nii = os.path.join(outputdir,'{}.nii.gz'.format(bpss_out))
                logger.info(fullpath_bpss_out_nii)
                outfiles = [fullpath_bpss_out_nii]
                if self._outfiles_skip(overwrite,outfiles):
                    continue
                # unlike most other scripts here, this one will fail instead of overwrite existing files
                # right now this is handled in runscript/bandpass.sbatch, but 
                #this might be needed in future
                #if os.path.exists(f):
                #    os.remove(files)
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','bandpass.sbatch'),
                    resid_out,
                    bpss_out,
                    outputdir,
                    mask,
                    self.conf.iproc.CODEDIR,
                    self.conf.iproc.SCRATCHDIR]
    
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list 
    
    def fs6_project_to_surface(self, overwrite=True):
        logger.debug('fs6_project_to_surface') 
        #TODO: output to different surfaces 
        #sesst is best t1   
    
        job_spec_list = []
        self.reset_steplog()

        sesst = self.conf.T1.T1_SESS
        subjid=self.conf.iproc.SUB
        for sessionid,sess in self.scans.sessions():
            for task_type,bold_scan in self.scans.tasks():
                scan_no = bold_scan['BLD']
                task = self.scans.task_dict[task_type]
                smooth = task['SMOOTHING']
                bold_no = "%03d" % int(scan_no)
                outputdir = os.path.join(self.conf.iproc.FS6DIR, sessionid, task_type)
                boldpath = os.path.join(self.conf.iproc.NAT111DIR, sessionid, task_type)
                if not os.path.exists(outputdir):
                    os.makedirs(outputdir) 
                
                surfdir = os.path.join(outputdir)
                bold = '{SESS}_bld{BOLDNO}_reorient_skip_mc_unwarp_anat'.format(SESS=sessionid,BOLDNO=bold_no)
                bold2 = bold + '_resid_bpss'
                bold3 = bold + '_wb_resid'
                # this is one of the files in the last batch. Not a comprehensive list of files
                outfiles = [os.path.join(surfdir,'{HEM}.{BOLD2}_fsaverage6_sm{SMOOTH}.nii.gz'.format(HEM='lh',SESS=sessionid,BOLD2=bold2,SMOOTH=smooth))]

                if self._outfiles_skip(overwrite,outfiles):
                    continue
    
                cmd=[os.path.join(self.conf.iproc.CODEDIR,'runscript','fs6_project_to_surf.sh'),
                    bold,
                    bold2,
                    bold3,
                    sesst,
                    boldpath,
                    surfdir,
                    self.conf.iproc.SCRATCHDIR,
                    smooth]
    
                logfile_base = self._io_file_fmt(cmd)
                job_spec_list.append(JobSpec(cmd,logfile_base,outfiles))
        self.scans.reset_default_sessionid()
        return job_spec_list
    
    ## Helpers
    def load_rmfile_dump(self,stagename,initialize_blank=False):
        # try to load a crash file. if none exists, load finished file from previous step.
        # must be passed the name of the previous step.

        # first check if this is a rerun:
        if os.path.exists(self.rm_final_filename):
            with open(self.rm_final_filename, 'r') as f:
                self.rmfiles=pickle.load(f)   
            logger.debug('Final rmfile found from a previous successful run of this step {}'.format(self.rm_final_filename))
            return
        if initialize_blank or self.args.blank_rmfiles:
            return
        try:
            assert(stagename)
            prior_final_dump_fname = os.path.join(self.conf.iproc.RMFILE_DUMP,'{STAGE}.final'.format(STAGE=stagename))
            with open(prior_final_dump_fname, 'r') as f:
                self.rmfiles = pickle.load(f)
            logger.debug('final file {} loaded'.format(prior_final_dump_fname))
        except (OSError,IOError,AssertionError) as e:
            logger.debug(e)
            try:
                dump_fname = self.rm_dump_filename
                with open(dump_fname, 'r') as f:
                    self.rmfiles=pickle.load(f)
            except (OSError,IOError) as e:
                logger.debug(e)
                logger.debug(dump_fname)
                logger.debug(prior_final_dump_fname)
                raise IOError('no appropriate dump file exists. Did you run the previous steps?')

    def _get_rmfiles(self,stepname):
        # this should work even in the case that the last two are "None"
        try: 
            rmfiles = self.rmfiles[stepname][self.scans.sessionid][self.scans.scan_no]
        except KeyError:
            rmfiles = []
        return rmfiles
        
    def _set_rmfiles(self,stepname,rmfiles):
        # this should work even in the case that the last two are "None"
        try:
            self.rmfiles[stepname][self.scans.sessionid][self.scans.scan_no] = rmfiles
        except KeyError:
            self.rmfiles[stepname]={}
            self.rmfiles[stepname][self.scans.sessionid] = {}
            self.rmfiles[stepname][self.scans.sessionid][self.scans.scan_no] = rmfiles
        # write to disk
        fname = self.rm_dump_filename
        with open(fname,'w') as f:
            pickle.dump(self.rmfiles,f)
 
    @staticmethod
    def _unwarp_direction_from_sidecar(fdir,sessid,boldno):
        ''' returns unwarp direction for FSL, to be used in fm_unw.sh
                sessid: sessid
                boldno: boldno, zero padded to 3 digits, string format'''
        BIDS_sidecar = os.path.join(fdir,'{SESS}_bld{BLDNO}.json'.format(SESS=sessid,BLDNO=boldno))
        BIDS_phase_direction = commons.get_json_entity(BIDS_sidecar,'PhaseEncodingDirection')
        if BIDS_phase_direction == 'j-':
            fsl_unwarpdirection = 'y-'
        elif BIDS_phase_direction == 'j':
            fsl_unwarpdirection = 'y'
        else:
            raise ValueError('unsupported phase encoding direction {}'.format(BIDS_phase_direction))
        return fsl_unwarpdirection

    def _io_file_fmt(self,run_cmd):
        # for scan_no, have this autodetect based on fieldmap/anat/bold flag and scan

        # get name of all runscripts, to get around fact that sometimes first element in list is a wrapper script
        runscripts = [s for s in run_cmd if '/runscript/' in s]
        scriptnames = [os.path.basename(s) for s in runscripts]
        scriptname = '_'.join(scriptnames)
        sessionid = self.scans.sessionid if self.scans.sessionid else 'SESSID'
        scan_no = self.scans.scan_no if self.scans.scan_no else 'SCAN_NO' 
        scan_name = self.scans.scan_name if self.scans.scan_name else 'SCAN_NAME' 
        outfile_base = os.path.join(self.conf.iproc.LOGDIR,
                "{SUB}_{SESSION_ID}_{SCAN_NO}_{TASK_ID}_{SCRIPTNAME}".format(
                SUB=self.conf.iproc.SUB,
                SESSION_ID=sessionid,
                SCAN_NO=scan_no,
                TASK_ID=scan_name,
                SCRIPTNAME=scriptname))
        return outfile_base
    
    def _outfiles_skip(self,overwrite,outfiles):

        outfiles_exist = {x:os.path.exists(x) for x in outfiles}
        existing_outfiles = [k for k,v in outfiles_exist.items() if v]
        nonexisting_outfiles = [k for k,v in outfiles_exist.items() if not v]
        
        if overwrite:
            return False 
        else:
            if not nonexisting_outfiles:
                logger.debug('outfiles exist {}. Job will not be added to job list.'.format(' '.join(existing_outfiles)))
                return True
            elif existing_outfiles: 
                logger.debug('outfiles are partially missing.')
                logger.debug('existing outfiles: {}'.format(' '.join(existing_outfiles)))
                logger.debug('missing outfiles: {}. Jobs will be added to job list for rerun.'.format(' '.join(nonexisting_outfiles)))
                return False
            else: # there are no existing outfiles
                logger.debug('outfiles do not exist {}. Jobs will be added to job list.'.format(' '.join(nonexisting_outfiles)))
                return False

