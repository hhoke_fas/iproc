import re
import os
import glob
import logging
import json
import subprocess as sp
import collections as col
import iproc.commons as commons
logger = logging.getLogger(__name__)

def match_scan_no_to_bids(bids_base,scans):
    for sessionid,sess in scans.sessions():
        #set corresponding BIDS subdir
        bids_sessionid = sanitize(sessionid)
        bids_sessionid_dirname = 'ses-{}'.format(bids_sessionid)
        # take care of fmap
        # compile list of SeriesNumber:(task,run)
        bids_func_fullpath = os.path.join(bids_base,bids_sessionid_dirname,'func')
        bids_json_fglob = 'sub-{SUB}_ses-{SES}_*_bold.json'.format(SUB=sess.subjid,SES=bids_sessionid)
        bids_json_glob = os.path.join(bids_func_fullpath,bids_json_fglob)
        scan_no_to_json = {}
        for json in glob.glob(bids_json_glob):
            # get series number from json, save in dict for later
            json_regex = '.*sub-{SUB}_ses-{SES}_task-(\w+)_run-([0-9]+)_bold.json'.format(SUB=sess.subjid,SES=bids_sessionid)
            bids_pair = re.match(json_regex,json).groups()
            series_no = get_json_entity(json,'SeriesNumber')
            scan_no_to_json[series_no]=bids_pair

        # Compile list of phase_SeriesNumber:BIDS_run_no
        bids_fmap_fullpath = os.path.join(bids_base,bids_sessionid_dirname,'fmap')
        bids_json_fglob = '*.json'.format(SUB=sess.subjid,SES=bids_sessionid)
        bids_json_glob = os.path.join(bids_fmap_fullpath,bids_json_fglob)

        fmap_jsons = glob.glob(bids_json_glob)
        if not fmap_jsons:
            logger.error('no JSON file found for {}'.format(bids_json_glob))
            raise IOError

        fmap_no_to_nifti = {}
        for json_fname in fmap_jsons:
            # get filenames by aquisition number
            series_no = get_json_entity(json_fname,'SeriesNumber')
                
            nifti_filename = json_fname.rstrip('.json') + '.nii.gz'
            if not os.path.exists(nifti_filename):
                raise(ValueError('{} does not exist'.format(nifti_filename)))
            existing_fmap = fmap_no_to_nifti.get(series_no)
            logger.debug('{} {} {}'.format(existing_fmap,json_fname,series_no))
            if not existing_fmap:
                fmap_no_to_nifti[series_no] = nifti_filename 
            elif type(existing_fmap) == str:
                fmap_no_to_nifti[series_no] = [existing_fmap,nifti_filename]
            else:
                fmap_no_to_nifti[series_no].append(nifti_filename)

        # Compile list of anat_SeriesNumber:BIDS_run_no
        bids_anat_fullpath = os.path.join(bids_base,bids_sessionid_dirname,'anat')
        bids_json_fglob = 'sub-{SUB}_ses-{SES}_*_T1w.json'.format(SUB=sess.subjid,SES=bids_sessionid)
        bids_json_glob = os.path.join(bids_anat_fullpath,bids_json_fglob)
        anat_no_to_json = {}     
        anat_jsons = glob.glob(bids_json_glob)
        if not anat_jsons:
            logger.error('no JSON file found for {}'.format(bids_json_glob))
            raise IOError
        for json in anat_jsons:
            # get series number for T1w anat from json, save in dict for later
            anat_regex = '.*sub-{SUB}_ses-{SES}_run-([0-9]+)_T1w.json'.format(SUB=sess.subjid,SES=bids_sessionid)
            anat_match = re.match(anat_regex,json)
            run_no = anat_match.group(1)
            series_no = get_json_entity(anat_match.string,'SeriesNumber')
            anat_no_to_json[series_no]=run_no
        try:
            sess.bids_anat_no = anat_no_to_json[sess.anat_no]
        except KeyError as e:
            logger.debug('anat_no_to_json:')
            logger.debug(anat_no_to_json)
            raise e

        ## Add this info into bold_scan objects
        for task_name,bold_scan in scans.tasks():
            scan_no = scans.scan_no 
            try:
                task,run = scan_no_to_json[scan_no] 
            except:
                logger.debug(scan_no_to_json)    
                raise
            if task != task_name:
                errname='BIDS taskname "{BIDS}" does not match boldscan task name "{TASK}" for sessid {SESS}, scan {SCAN}'.format(BIDS=task,TASK=task_name,SESS=sessionid,SCAN=scan_no)
                raise IOError(errname)
            bold_scan['BIDS_ID'] = run
            # correct naively-enumerated FMAP directories:
            fmap1_series_no = bold_scan['FIRST_FMAP']
            bold_scan['FMAP_DIR'] = 'FMAP'.format(fmap1_series_no)
            
        for fmap_dir,fmap_scan in scans.fieldmaps():
            
            
            fmap_scan['FIRST_BIDS_FNAME'] = load_fmap_file_to_scan(fmap_no_to_nifti,fmap_scan,'FIRST_FMAP')
            fmap_scan['SECOND_BIDS_FNAME'] = load_fmap_file_to_scan(fmap_no_to_nifti,fmap_scan,'SECOND_FMAP')

def load_fmap_file_to_scan(fmap_no_to_nifti,fmap_scan, scan_id):
    ''' 
    fmap_scan: fmap_scan from iproc/csvHandler
    scan_id: 'FIRST_FMAP' or 'SECOND_FMAP' 
    returns full path to fmap file, or list of such full paths
    '''

    fmap_series_no = fmap_scan[scan_id]
    logger.debug(fmap_scan)
    try:
        fmap_file = fmap_no_to_nifti[fmap_series_no]
    except KeyError as e:
        logger.debug('fmap_no_to_nifti')
        logger.debug(fmap_no_to_nifti)
        raise e
    fmap_scan['DIR'] = 'FMAP'.format(fmap_series_no)
    return fmap_file

def get_json_entity(json,entity):
    return str(commons.get_json_entity(json,entity))

class SplitTaskError(Exception):
    pass

def sanitize(s):
    regex = re.compile('[^a-zA-Z0-9]')
    return regex.sub('', s)

def split_task(s): 
    regex = re.compile('([a-zA-Z]+)_?(\d+)?') 
    match = regex.match(s) 
    if not match: 
        raise SplitTaskError('failed to split task "{0}"'.format(s)) 
    task,run = match.groups('1')
    return task,run
