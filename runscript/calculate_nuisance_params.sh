#!/bin/sh
set -xeou pipefail
# iProc_p5a.sh
# R. Braga 2018

RESID_IN=$1
CSF_TS=$2
CSF_MASK=$3
WM_TS=$4
WM_MASK=$5
WB_TS=$6
WB_MASK=$7
PHYS_TS=$8
MC_TS=$9
NUIS_TS=${10}
SCANG=${11}
NUIS_OUT=${12}
OUTDIR=${13}

tmpdir=$(mktemp --directory --tmpdir=${OUTDIR})

if [ "$IPROC_SRUN" == "YES" ] ; then
    #we're running in a srun-safe environment
srun -n 1 -c $SLURM_CPUS_PER_TASK parallel -j 3 --tmpdir=${tmpdir} <<EOF
fslmeants -i ${RESID_IN} -o ${CSF_TS} -m ${CSF_MASK}
fslmeants -i ${RESID_IN} -o ${WM_TS} -m ${WM_MASK}
fslmeants -i ${RESID_IN} -o ${WB_TS} -m ${WB_MASK}
EOF
else
parallel -j 3 --tmpdir=${tmpdir} <<EOF
fslmeants -i ${RESID_IN} -o ${CSF_TS} -m ${CSF_MASK}
fslmeants -i ${RESID_IN} -o ${WM_TS} -m ${WM_MASK}
fslmeants -i ${RESID_IN} -o ${WB_TS} -m ${WB_MASK}
EOF
fi
# note: this WB_TS is identical in all respects to the one produced
#by runscript/remove_WB_only.sh
printf "%0.3f\n" `cat ${WB_TS}` > ${WB_TS}
printf "%0.3f\n" `cat ${CSF_TS}` > ${CSF_TS}
printf "%0.3f\n" `cat ${WM_TS}` > ${WM_TS}

paste -d ' ' ${CSF_TS} ${WM_TS} ${WB_TS} | awk '$1!~/#/{printf("%10.3f%10.3f%10.3f\n", $1, $2, $3)}' > ${PHYS_TS}

paste -d ' ' ${MC_TS} ${PHYS_TS} | tr -s " " > ${NUIS_TS}

#TODO: different matlab version?
# this prevents the matlab program from failing silently on character-limit of 63, as it had before.
pushd $tmpdir
matlab -nojvm -nodesktop -nosplash -r "try e=0; format long g; nuis_ts=load('${NUIS_TS}'); xx=diff(zscore(detrend(nuis_ts))); x=zscore(detrend(nuis_ts)); xx=[zeros(size(xx(1,1:end)));xx]; xxx=[x xx]; dlmwrite('${tmpdir}/nuis_out.dat',xxx,'delimiter', ' ', 'precision',10); catch e=1; end; exit(e)"
rsync -av $tmpdir/nuis_out.dat "${NUIS_OUT}"
rm -rf ${tmpdir}
#we used to remove timeseries data, but from now on we'll keep it
