#!/bin/bash
set -xeou 

workdir_anat=${1}
sessionid=${2}
ANAT_SCAN_NO=${3}
codedir=${4}
xnat_alias=${5}
project=${6}

outfile=${7}
outfile_reorient=${8}

scratch_base=${workdir_anat}/${ANAT_SCAN_NO}
mkdir -p $scratch_base
ANAT_SCRATCHDIR=$(mktemp --directory --tmpdir=${scratch_base})
# automatically creates outdir and downloads dicoms into it
ArcGet.py -f flat -a ${xnat_alias} --label ${sessionid} --output-dir ${ANAT_SCRATCHDIR} --scans ${ANAT_SCAN_NO} --project ${project}
cd ${ANAT_SCRATCHDIR}
dcm=`ls | head -1`
dcm2niix -x i -z i -w 1 -f %i ${dcm}
# here is where BIDS directory would start
mv *.nii.gz tmp.nii.gz 

fslmaths tmp.nii.gz tmp.nii.gz -odt float
fslswapdim tmp.nii.gz -z -x y tmp2.nii.gz 

rsync --remove-source-files -aP "${ANAT_SCRATCHDIR}/tmp.nii.gz" ${outfile}
rsync --remove-source-files -aP "${ANAT_SCRATCHDIR}/tmp2.nii.gz" ${outfile_reorient}
