#!/bin/bash
set -xeou pipefail
# Fieldmap prep script, originally from fm.sh.
# separated from xnat download for modularity.
FDIR=$1
outfile=$2
datain=$3
b02b0=$4

# needed for the best version of topup
module load fsl/6.0.1-ncf

cd $FDIR

# copped from HCP. Check that the two unwarp images are the same size.
test "$(fslhd PA_img.nii.gz | grep '^dim[123]')" == "$(fslhd AP_img.nii.gz | grep '^dim[123]')"

fslmerge -t all_images.nii.gz PA_img.nii.gz AP_img.nii.gz
topup --imain=all_images.nii.gz --datain=$3 --config=$b02b0 --fout=topup_fmap.nii.gz --iout=se_epi_unwarped.nii.gz
#convert to radians by miltiplying by 2pi
fslmaths topup_fmap.nii.gz -mul 6.18 $outfile
#create magniture image and bet it
fslmaths se_epi_unwarped.nii.gz -Tmean mag_img.nii.gz
bet2 mag_img.nii.gz mag_img_brain.nii.gz -m
fslmaths $FDIR/mag_img_brain -ero $FDIR/mag_img_brain_ero
