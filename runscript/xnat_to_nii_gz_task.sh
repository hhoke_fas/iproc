#!/bin/bash
set -xeou 

workdir=${1}
sessionid=${2}
task_scan_no=${3}
codedir=${4}
xnat_alias=${5}
project=${6}

outfile=${7}

SKIP=${8}
NUMVOL=${9}

bold_no=$(printf %03d ${task_scan_no})
fname_base=${sessionid}_bld${bold_no}

OUTDIR=$(dirname ${outfile})
scratch_base=${workdir}/${task_scan_no}/
mkdir -p $scratch_base
SCRATCHDIR=$(mktemp --directory --tmpdir=${scratch_base})

ArcGet.py -f flat -a ${xnat_alias} --label ${sessionid} --output-dir ${SCRATCHDIR} --scans ${task_scan_no} --project ${project}
cd ${SCRATCHDIR}
dcm=`ls | head -1`
# get echo time from dicom
EchoTime_keypair='0018 0081'
EchoTime=$(mri_probedicom --i $dcm --t $EchoTime_keypair | cut -d* -f1)
# EchoTime multiplied by conversion factor from MS to Sec
EchoTime_sec=$(bc -l <<< "((1/1000) * $EchoTime)") 
precision=4
EchoTime_sec=$(/usr/bin/printf "%.*f\n" $precision $EchoTime_sec)
if [ -z "$EchoTime_sec" ]; then
    exit 1 
fi
dcm2niix -x i -z i -w 1 -f %i ${dcm}
# here is where BIDS directory would start
mv *.nii.gz tmp.nii.gz

# check that we have enough time points  
total_timepoints=$(fslnvols "${SCRATCHDIR}/tmp.nii.gz" )
((requested_timepoints=${SKIP}+${NUMVOL}))
if [ "$total_timepoints" -lt "$requested_timepoints" ]; then
    echo "total timepoints are less than the sum of skipped and the numvol set in the task csv."
    exit 1
fi
# for now, just to get the BIDS info
dcm2niix -x i -z i -w 1 -s y -f ${fname_base} -o "${OUTDIR}" $dcm 
# we only want the JSON
rm "${OUTDIR}"/${fname_base}.nii.gz
BandwidthPerPixelPhaseEncode_keypair='0019 1028'
BandwidthPerPixelPhaseEncode=$(mri_probedicom --i $dcm --t $BandwidthPerPixelPhaseEncode_keypair)
AcquisitionMatrixText_keypair='0051 100b'
#This is usually the case, but is not guaranteed according to https://lcni.uoregon.edu/kb-articles/kb-0003
MatrixSizePhase=$(mri_probedicom --i $dcm --t $AcquisitionMatrixText_keypair | cut -d* -f1)
EffectiveEchoSpacing=$(bc -l <<< "(1/($BandwidthPerPixelPhaseEncode * $MatrixSizePhase))") 
precision=5
EffectiveEchoSpacing=$(/usr/bin/printf "%.*f\n" $precision $EffectiveEchoSpacing)
if [ -z "$EffectiveEchoSpacing" ]; then
    exit 1
fi
fslmaths tmp.nii.gz tmp.nii.gz -odt float #convert to float
fslroi ${SCRATCHDIR}/tmp ${SCRATCHDIR}/tmp_reorient_skip.nii.gz ${SKIP} ${NUMVOL} #trim extra timepoints
rsync --remove-source-files -aP "${SCRATCHDIR}/tmp_reorient_skip.nii.gz" ${outfile}
#write echo time to a file
echo $EchoTime_sec > $OUTDIR/${fname_base}_echoTime.sec
echo $EffectiveEchoSpacing > $OUTDIR/${fname_base}_dwellTime.sec
