#!/bin/bash
set -xeou pipefail
#
#Usage: iProc_fs.sh <subjid> <scan_no> <anat_scan_no> <subjects_dir> <basedir> <codedir>
# e.g.  iProc_fs.sh 5MQ58 170710_HTP02017 010 

# Written by R. Braga - Feb 2018

SUBJECT=$1
SESSION=$2
MPR_REORIENT=$3
SUBJECTSDIR=$4
FSAVERAGE6=$5
SCRATCHDIR=$6
CODEDIR=$7

cpus=`${CODEDIR}/executorcli.py --cpus-per-node`
export OMP_NUM_THREADS=${cpus}

origdir=$SCRATCHDIR/$SUBJECT/${SESSION}/mri/orig
if [ -d "$origdir" ]; then 
    rm -Rf $origdir 
fi

mkdir -m 750 -p $origdir

mri_convert $MPR_REORIENT $origdir/001.mgz

recon-all -sd $SCRATCHDIR/${SUBJECT} -s ${SESSION} -all -custom-tal-atlas RLB700_atlas_as_orig -parallel -openmp $OMP_NUM_THREADS

sleep 3

echo "Copying /scratch/${SUBJECT}/${SESSION}/ contents to ${SUBJECTSDIR}/${SESSION}/"
mkdir -p ${SUBJECTSDIR}/${SESSION}/
rsync --remove-source-files -aP $SCRATCHDIR/${SUBJECT}/${SESSION}/* ${SUBJECTSDIR}/${SESSION}/

ln -sfT ${FSAVERAGE6} $SUBJECTSDIR/fsaverage6
