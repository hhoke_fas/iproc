#!/usr/bin/env python

import os
import sys
import shutil
import logging
import json
import re
import argparse as ap
import tempfile as tf 
import subprocess as sp

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def main():
    parser = ap.ArgumentParser()
    parser.add_argument('--input',
        help='Input BIDS file')
    parser.add_argument('--output',
        help='Output destination')
    parser.add_argument('--sec_base',
        help='basename for .sec files')
    parser.add_argument('--skip', type=int,
        help='Volumes to skip')
    parser.add_argument('--num-vols', type=int,
        help='Volumes to keep (after skipped)')
    parser.add_argument('--work-dir',
        help='Working directory')
    args = parser.parse_args()
    
    args.input = os.path.expanduser(args.input)
    args.output = os.path.expanduser(args.output)

    output_basename = os.path.basename(args.output)
    try:
        os.makedirs(args.work_dir)
    except OSError as e:
        if e.errno == 17:
            pass
        else:
            raise
    
    # create a temporary directory for this process
    tempd = tf.mkdtemp(dir=args.work_dir)
    logger.info('created temporary working directory: %s', tempd)
    os.chdir(tempd)

    # force the input file to float
    forced = os.path.join(tempd, 'tmp.nii.gz')
    force_dt(args.input, forced, dtype='float')

    # reorient the forced file
    skipped = os.path.join(tempd, output_basename)
    roi(forced, skipped, a=args.skip, b=args.num_vols)

    # move files to final destination
    logger.info('moving %s to %s', skipped, args.output)
    shutil.move(skipped, args.output)
    
    # move BIDS json
    json_fname = translate_json(args.input,args.sec_base)
    renamed_json = os.path.join(args.output,'{}.json'.format(args.sec_base))
    shutil.copy(json_fname,renamed_json)

    # remove the temporary directory
    shutil.rmtree(tempd)

def force_dt(input, output, dtype='float'):    
    cmd = [
        'fslmaths',
        input,
        output,
        '-odt', dtype
    ]
    logger.info(cmd)
    sp.check_output(cmd)

def roi(input, output, a, b):
    cmd = [
        'fslroi',
        input,
        output,
        str(a),
        str(b)
    ]
    logger.info(cmd)
    sp.check_output(cmd)

def translate_json(input, fname_base):

    nifti_basename = re.match('(.*)\.nii\.gz',input).group(1) or re.match('(.*)\.nii',input).group(1)
    if not nifti_basename:
        raise ValueError('{} does not end with .nii.gz or .nii'.format(input))
    json_name = nifti_basename + '.json'
    with open(json_name) as j:
        scan_data = json.load(j)
        echoTime = str(scan_data['EchoTime']) 
        dwellTime = str(scan_data['EffectiveEchoSpacing']) 
        # not going to use here, but want to make sure it's in json
        phase_direction = scan_data['PhaseEncodingDirection']
    echoTime_fname = fname_base + '_echoTime.sec'
    dwellTime_fname = fname_base + '_dwellTime.sec'

    with open(echoTime_fname,'w') as f:
        f.write(echoTime)
        logger.info('{} written to {}'.format(echoTime,echoTime_fname))

    with open(dwellTime_fname,'w') as f:
        f.write(dwellTime)
        logger.info('{} written to {}'.format(dwellTime,dwellTime_fname))
    return json_name
        
if __name__ == '__main__':
    main()

