#!/bin/bash
set -xeou
#Apply fieldmap correction # iProc_fm.sh 4GP28 170712_HTP02020 33 34 /ncf/cnl03/25/users/DN2/fm_test

# echo every line to stdout as it runs, and exit if any command returns nonzero,
# even if there is a pipe


sessionid=$1
first_FM_no=$2
second_FM_no=$3
FDIR=$4
codedir=$5
xnat_alias=$6
project=$7
outfile=$8
preptool=topup

# change later if it comes up
b02b0=$codedir/configs/b02b0.cnf

cd $FDIR


dir1=PA_img
dir2=AP_img

# # # # 
#Download Magnitude image and convert to nifti
ArcGet.py -f flat -a ${xnat_alias} -l $sessionid -p $project --scans $first_FM_no -o $dir1
cd $FDIR/$dir1
dcm=$(ls | head -1)
dcm2niix -x i -z i -w 1 -f %i ${dcm}
mv `ls *.nii.gz | tail -1` ../$dir1.nii.gz #Second image is what we want

# Get info here, while in subdirectory
BandwidthPerPixelPhaseEncode_keypair='0019 1028'
BandwidthPerPixelPhaseEncode=$(mri_probedicom --i $dcm --t $BandwidthPerPixelPhaseEncode_keypair)
AcquisitionMatrixText_keypair='0051 100b'
#This is usually the case, but is not guaranteed according to https://lcni.uoregon.edu/kb-articles/kb-0003
MatrixSizePhase=$(mri_probedicom --i $dcm --t $AcquisitionMatrixText_keypair | cut -d* -f1)

#Download Phase image and convert to nifti
cd $FDIR
ArcGet.py -f flat -a ${xnat_alias} -l $sessionid -p $project -s $second_FM_no -o $dir2
cd $FDIR/$dir2
dcm=`ls | head -1`
dcm2niix -x i -z i -w 1 -f %i ${dcm}
mv *.nii.gz ../$dir2.nii.gz

cd $FDIR

# compute and write dicom info
EffectiveEchoSpacing=$(bc -l <<< "(1/($BandwidthPerPixelPhaseEncode * $MatrixSizePhase))") 
TotalReadoutTime=$(bc -l <<< "($MatrixSizePhase - 1) * $EffectiveEchoSpacing")
datain=$FDIR/topupDatain.dat
# in case there is already such a file, overwrite it
rm -f $datain
echo "0 -1 0 $TotalReadoutTime" >> $datain
echo "0 1 0 $TotalReadoutTime" >> $datain
# we're assuming that the PA and AP files have all the same scan parameters here, e.g. TotalReadoutTime
${codedir}/runscript/fmap_${preptool}_prep.sh ${FDIR} ${outfile} ${datain} ${b02b0}

rm -r $FDIR/$dir2
rm -r $FDIR/$dir1
