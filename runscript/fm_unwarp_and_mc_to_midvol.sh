#!/bin/bash
set -xeou pipefail

MC_IN=${1}
MC_OUT=${2}
TARGET=${3}
FLIRT_OUT=${4}
FLIRT_MAT_OUT=${5}
SCAN_TYPE=${6}
NUMVOL=${7}
OUTDIR=${8}
CODEDIR=${9}
FM_SESSID=${10}
FM_BOLDNO=${11}
MIDVOL=${12}
FMdir=${13}
REGRESSORS_MC_DAT_OUT=${14}
DEST_DIR=${15}
WARP_DIR=${16}
unwarp_direction=${17}
rmfiles=${18:-''}

MIDVOL_UNWARP=${MIDVOL}_unwarp

mcflirt -in ${MC_IN} -out ${MC_OUT} -mats -plots -rmsrel -rmsabs -report 
fslroi ${MC_OUT} ${MIDVOL} `expr ${NUMVOL} / 2` 1 
# not going to pass on any warpfiles
${CODEDIR}/modwrap.sh 'module load fsl/4.0.3-ncf' 'module load fsl/5.0.4-ncf' ${CODEDIR}/runscript/fm_unw.sh ${FM_SESSID} ${FMdir} ${MIDVOL} ${MIDVOL_UNWARP} ${FM_BOLDNO} ${DEST_DIR} ${WARP_DIR} ${unwarp_direction}
flirt -in ${MIDVOL_UNWARP} -ref ${TARGET} -out ${FLIRT_OUT} -omat ${FLIRT_MAT_OUT} -bins 256 -cost corratio -searchrx -180 180 -searchry -180 180 -searchrz -180 180 -dof 12 -interp trilinear 
${CODEDIR}/runscript/p2a.sh ${OUTDIR} ${SCAN_TYPE} ${NUMVOL} ${REGRESSORS_MC_DAT_OUT}

if [ -n "$rmfiles" ]; then
    for f in $rmfiles;do
        rm -rf "$f"
    done
fi
