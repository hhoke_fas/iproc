#!/bin/bash
set -xeou pipefail

SUBJECTS_DIR=${1}
SESST=${2}
TARGDIR=${3}
mpr_reorient=${4}

#these files may not exist at the time, but this was the most convenient place to put this
#TODO: make relative
ln -sf ${TARGDIR}/${SESST}_mpr_reorient.nii.gz ${TARGDIR}/mpr_reorient.nii.gz 
ln -sf ${TARGDIR}/${SESST}_mpr_reorient_brain.nii.gz ${TARGDIR}/mpr_reorient_brain.nii.gz 
ln -sf ${TARGDIR}/${SESST}_mpr_reorient_brain_mask.nii.gz ${TARGDIR}/mpr_reorient_brain_mask.nii.gz 
ln -sf ${TARGDIR}/${SESST}_mpr.nii.gz ${TARGDIR}/mpr.nii.gz 

mpr=${TARGDIR}/${SESST}_mpr.nii.gz
mri_convert $SUBJECTS_DIR/$SESST/mri/T1.mgz ${mpr}
fslreorient2std ${mpr} ${mpr_reorient}
