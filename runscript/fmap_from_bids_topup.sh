#!/bin/bash
set -xeou
#Apply fieldmap correction # iProc_fm.sh 4GP28 170712_HTP02020 33 34 /ncf/cnl03/25/users/DN2/fm_test

# echo every line to stdout as it runs, and exit if any command returns nonzero,
# even if there is a pipe


first_FM_no=$1
second_FM_no=$2
FDIR=$3
codedir=$4
TotalReadoutTime=$5
outfile=$6

preptool=topup

# change later if it comes up
b02b0=$codedir/configs/b02b0.cnf

pushd $FDIR

# compute and write dicom info
datain=$FDIR/fmap${first_FM_no}_topupDatain.dat
# in case there is already such a file, overwrite it
rm -f $datain
echo "0 -1 0 $TotalReadoutTime" >> $datain
echo "0 1 0 $TotalReadoutTime" >> $datain
# we're assuming that the PA and AP files have all the same scan parameters here, e.g. TotalReadoutTime
${codedir}/runscript/fmap_${preptool}_prep.sh ${FDIR} ${outfile} ${datain} ${b02b0}

