#!/usr/bin/env python
'''
Main iproc engine. This one script runs the entire data preprocessing pipeline.
'''

import shutil
import time
import os
import sys
import glob
import logging
import logging.config
import collections
import pickle
import datetime as dt
import subprocess as sp
import argparse as ap

import steps as iProcSteps
from iproc import conf,csvHandler
from iproc.config import ConfigError
import iproc.commons as commons
import iproc.executors as executors
import iproc.qc as qc
import iproc.bids as bids

logger = logging.getLogger(os.path.basename(__file__))
##
# stages
##

def setup(steps,args):

    ########################################
    # STEP 1: get ANAT, BOLD, and Field Maps
    ########################################

    # TODO: none of these steps produce rmfiles, these are totally unneeded.
    rmfiles = []

    if args.bids:
        job_spec_list = steps.fmap_from_bids(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps, throttle=10, **args.cluster['ingest_fieldmap'])

        job_spec_list = steps.anat_from_bids(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps, throttle=10, **args.cluster['ingest_anat'])

        job_spec_list = steps.func_from_bids(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps,throttle=10,**args.cluster['ingest_task'])

    else:
        job_spec_list = steps.xnat_to_nii_gz_fieldmap(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps, throttle=10, **args.cluster['ingest_fieldmap'])

        job_spec_list = steps.xnat_to_nii_gz_anat(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps, throttle=10, **args.cluster['ingest_anat'])

        job_spec_list = steps.xnat_to_nii_gz_task(overwrite=args.overwrite)
        rmfiles += execute(args.executor, job_spec_list,steps,throttle=10,**args.cluster['ingest_task'])

    job_spec_list = QC_fmap(steps,args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps,**args.cluster['fmap_qc'])
    ########################################
    # Kick off recon-all freesurfer
    ########################################

    job_spec_list = steps.recon_all(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps,**args.cluster['recon_all'])

    logger.info("Check quality of Surface Estimation using the following commands:")
    for sessionid,_ in steps.scans.sessions():
        anat_vol = os.path.join(conf.fs.SUBJECTS_DIR,sessionid,'mri','T1.mgz')
        logger.info("surface/T1 match check:")
        logger.info("freeview -v {ANAT} -f {SUBJECTS_DIR}/{SESS}/surf/lh.pial:edgethickness=1 {SUBJECTS_DIR}/{SESS}/surf/lh.white:edgecolor=blue:edgethickness=1 {SUBJECTS_DIR}/{SESS}/surf/rh.pial:edgethickness=1 {SUBJECTS_DIR}/{SESS}/surf/rh.white:edgecolor=blue:edgethickness=1".format(ANAT=anat_vol,SUBJECTS_DIR=conf.fs.SUBJECTS_DIR,SESS=sessionid))
        logger.info("spherical registration fsaverage alignment check:")
        logger.info("freeview -f {SUBJECTS_DIR}/{SESS}/surf/lh.sphere.reg:overlay=lh.sulc:{SUBJECTS_DIR}/{SESS}/surf/lh.sulc:overlay_threshold=0,1 -viewport 3d -camera Elevation 60".format(SUBJECTS_DIR=conf.fs.SUBJECTS_DIR,SESS=sessionid))
        logger.info("The threshold is either 0,1 or 0,10 (depending on the individual, and this can be adjusted)")
    logger.info("compare this to fsaverage6:")
    logger.info("freeview -f {SUBJECTS_DIR}/fsaverage6/surf/lh.sphere.reg:overlay=lh.sulc:{SUBJECTS_DIR}/fsaverage6/surf/lh.sulc:overlay_threshold=0,1 -viewport 3d -camera Elevation 60".format(SUBJECTS_DIR=conf.fs.SUBJECTS_DIR))

    logger.info("Scans downloaded from XNAT and freesurfer recon_all completed")
    logger.info("Define T1_SESS in [T1] section of conf file from the best session.")
    return rmfiles

def QC_fmap(steps,overwrite):

    #TODO: would be nice to just get a list made from steps.py
    merge_filter = 'reorient_skip'
    # set up qc objects
    qc_ax = qc.qc_pdf_maker(conf,'ax')
    qc_sag = qc.qc_pdf_maker(conf,'sag')
    # create the t1 pages
    ax_slicer = {'window_dims':'0 100 0 100 5 55'.split(' '),
                        # <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
                        'sample': '1', # nth image to sample
                        'width': 7} # Number of images across, I think

    sag_slicer = {'window_dims':'0 100 0 100 15 60'.split(' '),
                        # <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
                        'sample': '1', # nth image to sample
                        'width': 8} # Number of images across, I think

    task_ax_slicer = ax_slicer
    task_sag_slicer = sag_slicer
    for sessionid,_ in steps.scans.sessions():
        # scanno:scan_object for bold scans
        bold_dict = {int(bold_scan['BLD']):bold_scan for _,bold_scan in steps.scans.tasks()}
        for fmap_dir,fmap_scans in steps.scans.fieldmaps():
            fmap1_no = fmap_scans['FIRST_FMAP']
            fmap1_no_pad = "%03d" % int(fmap1_no)
            # add BOLD page
            bold_scan = nearby_bold(bold_dict,int(fmap1_no))
            scan_no = bold_scan['BLD']
            task_type = bold_scan['TYPE']
            bold_no = "%03d" % int(scan_no)
            spacename = "%s_bld%s_%s" % (sessionid,bold_no,merge_filter)
            infile = os.path.join(conf.iproc.NATDIR,sessionid,task_type,spacename+'.nii.gz')
            task_page_sag = qc.page(infile,task_sag_slicer)
            task_page_ax = qc.page(infile,task_ax_slicer)
            qc_sag.pages.append(task_page_sag)
            qc_ax.pages.append(task_page_ax)
            # add fmap page
            fmap_dirname = os.path.join(steps.conf.iproc.NATDIR,sessionid,fmap_dir)
            target_fmap_nii = '{fdir}/{sessionid}_{fmap1_no}_fieldmap.nii.gz'.format(fdir=fmap_dirname,sessionid=sessionid,fmap1_no=fmap1_no_pad)
            fmap_page_sag = qc.page(target_fmap_nii,task_sag_slicer)
            fmap_page_ax = qc.page(target_fmap_nii,task_ax_slicer)
            qc_sag.pages.append(fmap_page_sag)
            qc_ax.pages.append(fmap_page_ax)
    returnval = []
    ax_job = qc_ax.produce_pdf('fmap',save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if ax_job:
        returnval.append(ax_job)
    sag_job = qc_sag.produce_pdf('fmap',save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if sag_job:
        returnval.append(sag_job)
    logger.debug(returnval)
    return returnval

def nearby_bold(bold_dict,scan_no):
    # Takes in a bold dict which indexes bold_scan objects by 
    # integer-form aquisition number
    # also takes in scan_no, an integer 
    i=scan_no
    # get the lowest scan number in bold list
    first_scan = int(min(list(bold_dict.keys())))
    while i>first_scan:
        i=i-1
        try:
            bold_scan = bold_dict[i]  
            return bold_scan
        except KeyError:
            continue
    i=scan_no
    # get the highest scan number in bold list
    last_scan = int(max(list(bold_dict.keys())))
    while i<last_scan:
        i=i+1
        try:
            bold_scan = bold_dict[i]  
            return bold_scan
        except KeyError:
            continue
    logger.error('{} not found in {}'.format(scan_no,bold_dict))
    raise KeyError
        
def check_bet(steps,args):
    # before you run this stage, be sure to run freesurfer, check the quality of the output

    rmfiles=[]
    #steps.load_rmfile_dump('setup')
    # STEP 1: Calculate warp from mean BOLD to T1
    job_spec_list = steps.sesst_prep(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps,**args.cluster['sesst_prep'])

    mpr_bet()

    # not needed, as this stage produces no rmfiles
    #os.rename(steps.rm_dump_filename,steps.rm_final_filename)
    return rmfiles

def mpr_bet():
    betcmd = "bet {TARGDIR}/{SESST}_mpr_reorient {TARGDIR}/{SESST}_mpr_reorient_brain -m -R -v".format(TARGDIR=conf.template.TEMPLATE_DIR,SESST=conf.T1.T1_SESS)
    fslview = "fslview {TARGDIR}/{SESST}_mpr_reorient*".format(TARGDIR=conf.template.TEMPLATE_DIR,SESST=conf.T1.T1_SESS)
    explain_bet(betcmd,fslview)

def unwarp_motioncorrect_align(steps,args):

    rmfiles = []
    # still needed, as we might need to load from a crashed unwarp_motioncorrect_align run.
    nat_brain = '{TARGDIR}/mpr_reorient_brain.nii.gz'.format(TARGDIR=conf.template.TEMPLATE_DIR)
    try:
        os.stat(nat_brain)
    except OSError:
        logger.error('{} not found, '.format(nat_brain))
        mpr_bet()
        raise IOError('{} not found, quitting'.format(nat_brain))
    steps.load_rmfile_dump(None,initialize_blank=True)
    ########################################
    # Step 1: (substeps should be sequential)
    ########################################
    # a) Extract middle volume of scan from Session1 acquired closest in time to the field map
    job_spec_list = steps.fslroi_reorient_skip(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['fslroi_reorient_skip'])

    # b) Extract middle volume of scan from Session1 acquired closest in time to the field map
    # and unwarp midvol image using fieldmap
    job_spec_list = steps.fm_unwarp_midvol(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['fm_unwarp_midvol'])

    # temp for fm testing

    # c) Upsample to create midvol target:
    job_spec_list = steps.create_upsamped_midvol_target(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['create_upsampled_midvol_target'])

    ########################################
    # STEP 2: Motion correct each run with mcflirt, and register all runs to
    # unwarped midvol target produced from the midvol of midvol_sess specified in conf
    ########################################

    job_spec_list = steps.fm_unwarp_and_mc_to_midvol(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['fm_unwarp_and_mc_to_midvol'])

    ########################################
    # STEP 3: Calculate warp to mean BOLD midvol template produced by averaging
    # the output of fm_unwarp_and_mc_to_midvol.
    ########################################

    # create mean bold midvol template
    merge_filter = 'reorient_skip_mc_unwarp_midvol_to_midvoltarg'
    # flirt output from fm_unwarp_and_mc_to_midvol
    merge_glob = conf.iproc.NATDIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    # was standard_midvol_on_midvoltarg_allscansmean
    # conf.template.midvols mean is used in the next step, so is defined in main
    job_spec_list = steps.fslmerge_meantime(conf.template.midvols_mean,globfiles_tomerge,overwrite=args.overwrite)
    logger.info('merging BOLD scans')
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_midvols'])

    job_spec_list = steps.align_to_midvol_mean(overwrite=args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['align_to_midvol_mean'])

    ## end of computational steps for unwarp_motioncorrect_align stage

    ########################
    # QC1 mean of bold midvol and fm_unwarp_and_mc_to_midvol output to pdf
    ########################

    qc_pdfs = []
    job_spec_list = QC_unwarp_motioncorrect_align(steps,merge_filter,conf.template.midvols_mean,args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['midvol_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]

    ########################
    # QC2 checks output of align_to_midvol_mean
    ########################

    # get mean of flirt output from align_to_midvol_mean, which is the
    # product of registering the BOLD to the mean of BOLD midvols
    merge_filter='on_midmean'
    merge_glob = conf.iproc.NATDIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    # was standard_midvol_on_allscansmean.nii.gz
    # going to just call meanbold, don't think this is used later on in the pipeline anyway
    template_fname = 'meanbold_midvols_on_midmean.nii.gz'
    conf.template.meanbold = os.path.join(conf.template.TEMPLATE_DIR,template_fname)

    job_spec_list = steps.fslmerge_meantime(conf.template.meanbold,globfiles_tomerge,overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_midmean'])

    job_spec_list = QC_unwarp_motioncorrect_align(steps,merge_filter,conf.template.meanbold,args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['midmean_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]

    for qc_pdf in qc_pdfs:
        logger.info(qc_pdf)
    logger.info('done with unwarp_motioncorrect_align preprocessing! Check QC PDFs above by flipping through and looking for major changes in the location of sulci and gyri')
    if os.path.exists(steps.rm_dump_filename):
        # if this is a rerun, we would want to skip that bc the rm_dump_filename will not exist
        os.rename(steps.rm_dump_filename,steps.rm_final_filename)
    return rmfiles

def QC_unwarp_motioncorrect_align(steps, merge_filter,template,overwrite):

    # set up qc objects
    qc_ax = qc.qc_pdf_maker(conf,'ax')
    qc_sag = qc.qc_pdf_maker(conf,'sag')
    # create the t1 pages
    ax_slicer = {'window_dims':'10 160 10 160 20 105'.split(' '),
                        # <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
                        'sample': '1', # todo: what is this
                        'width': 14} # Number of images across, I think
    page_ax = qc.page(template,ax_slicer)
    qc_ax.pages.append(page_ax)

    # changed from zmin=30
    sag_slicer = ax_slicer
    page_sag = qc.page(template,sag_slicer)
    qc_sag.pages.append(page_sag)

    task_ax_slicer = ax_slicer
    task_sag_slicer = sag_slicer
    for sessionid,_ in steps.scans.sessions():
        for task_type,bold_scan in steps.scans.tasks():
            scan_no = bold_scan['BLD']
            bold_no = "%03d" % int(scan_no)
            spacename = "%s_bld%s_%s" % (sessionid,bold_no,merge_filter)
            infile = os.path.join(conf.iproc.NATDIR,sessionid,task_type,spacename+'.nii.gz')
            task_page_sag = qc.page(infile,task_sag_slicer)
            task_page_ax = qc.page(infile,task_ax_slicer)
            qc_sag.pages.append(task_page_sag)
            qc_ax.pages.append(task_page_ax)
    returnval = []
    ax_job = qc_ax.produce_pdf(merge_filter,save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if ax_job:
        returnval.append(ax_job)
    sag_job = qc_sag.produce_pdf(merge_filter,save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if sag_job:
        returnval.append(sag_job)
    return returnval

##
# warp point
##
# past this point we're working with output spaces
def T1_warp_and_mask(steps,args):

    steps.load_rmfile_dump('unwarp_motioncorrect_align')
    rmfiles = []
    ########################################
    job_spec_list = steps.bbreg(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['bbreg'])

    logger.info('Calculate warp from native T1 to MNI space')
    job_spec_list = steps.compute_T1_MNI_warp(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['compute_T1_MNI_warp'])

    logger.info('register MNI csf and wm masks to T1 space')
    job_spec_list = steps.reg_MNI_CSF_WM_to_T1(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['reg_MNI_CSF_WM_to_T1'])

    fnirt_bet_reminder()
    #os.rename(steps.rm_dump_filename,steps.rm_final_filename)
    # ( no rmfiles are added during this stage, so no crashfile is created)
    return rmfiles

def fnirt_bet_reminder():
    betcmd = 'bet {TARGDIR}/anat_mni_underlay.nii.gz {TARGDIR}/anat_mni_underlay_brain -m -R -v'.format(TARGDIR=conf.template.TEMPLATE_DIR)
    fslviewcmd = 'fslview {TARGDIR}/anat_mni_underlay*'.format(TARGDIR=conf.template.TEMPLATE_DIR)
    explain_bet(betcmd,fslviewcmd)

def explain_bet(betcmd,fslviewcmd):
    logger.info('1) run the following command:')
    logger.info(betcmd)
    logger.info('2) copy down the final c-of-g values, call them x y z')
    logger.info('3) check output and adjust for a new bet command.')
    logger.info('   You want as little non-brain included in the mask as you can, without missing any brain.')
    logger.info('')
    logger.info(fslviewcmd)
    logger.info('')
    logger.info('4) adjust for a new bet command')
    logger.info('   for example:')
    new_betcmd = betcmd + ' -c x y z+10 '
    logger.info(new_betcmd)
    logger.info('')
    logger.info("Feel free  to try other options from `bet --help`.")
    logger.info("Adjusting the center, and occasionally -f, has given us the best results")
    logger.info('')
    logger.info('5) finally, record your changes! A good idea is to store them in a text file in your templates folder named bet.txt')
    logger.info('copy your own commands into the sample template below')
    logger.info('echo "YOUR BET CMD" >> /YOUR_OUTDIR/cross_session_maps/templates/logs/bet.txt')

def combine_and_apply_warp(steps,args):

    # load from unwarp_motioncorrect_align, T1_warp_and_mask doesn't add any rmfiles
    steps.load_rmfile_dump('unwarp_motioncorrect_align')
    rmfiles = []
    ########################################
    mni_underlay_brain = '{TARGDIR}/anat_mni_underlay_brain.nii.gz'.format(TARGDIR=conf.template.TEMPLATE_DIR)
    if not os.path.exists(mni_underlay_brain):
        logger.error('{} not found, quitting'.format(mni_underlay_brain))
        fnirt_bet_reminder()
        raise IOError('{} not found, quitting'.format(mni_underlay_brain))

    logger.info('dilate mask to use in reducing volume size')
    job_spec_list = steps.size_brainmask(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['size_brainmask'])

    logger.info('combine warps to project data to output T1 space')
    parallel_job_spec_list = steps.combine_warps_parallel(anat_space='T1',overwrite=args.overwrite)

    # this is where  we transfer to NAT111
    post_job_spec_list = steps.combine_warps_post(overwrite=args.overwrite)

    # package cluster args for dependent jobs
    kwargs = args.cluster['combine_warps_post_anat']
    if kwargs['RUNMODE'] == 'run':
        del kwargs['RUNMODE']
        assert len(parallel_job_spec_list) == len(post_job_spec_list)
        for parallel_job,post_job in zip(parallel_job_spec_list,post_job_spec_list):
            parallel_job.afterok.append((post_job,kwargs))

    rmfiles += execute(args.executor,parallel_job_spec_list,steps,throttle=10,**args.cluster['combine_warps_parallel_anat'])
    anat_anatdir = os.path.join(conf.iproc.NAT111DIR,'ANAT')
    if not os.path.isdir(anat_anatdir):
        os.makedirs(anat_anatdir)
    mpr_reorient = os.path.join(conf.template.TEMPLATE_DIR,'mpr_reorient.nii.gz')
    anat_link = ['cp', '-f',mpr_reorient,anat_anatdir]
    commons.check_call(anat_link)

    logger.info('combine warps to project data to MNI space')
    parallel_job_spec_list = steps.combine_warps_parallel(anat_space='MNI',overwrite=args.overwrite)

    # this is where we transfer to MNI111
    post_job_spec_list = steps.combine_warps_post_MNI(overwrite=args.overwrite)
    kwargs = args.cluster['combine_warps_post_mni']
    if kwargs['RUNMODE'] == 'run':
        del kwargs['RUNMODE']
        assert(len(parallel_job_spec_list) == len(post_job_spec_list))
        for parallel_job,post_job in zip(parallel_job_spec_list,post_job_spec_list):
            parallel_job.afterok.append((post_job,kwargs))
    rmfiles += execute(args.executor,parallel_job_spec_list,steps,throttle=10,**args.cluster['combine_warps_parallel_mni'])

    # copy mni anat to output dir
    mni_anatdir = os.path.join(conf.iproc.MNI111DIR,'ANAT')
    if not os.path.isdir(mni_anatdir):
        os.makedirs(mni_anatdir)
    anat_mni_underlay = os.path.join(conf.template.TEMPLATE_DIR,'anat_mni_underlay.nii.gz')
    anat_link = ['cp', '-f',anat_mni_underlay,mni_anatdir]
    commons.check_call(anat_link)

    #########################
    # QC PDF generation
    #########################
    qc_pdfs = []
    ## NAT111
    # mean_out from combine_warps_post
    merge_filter = 'reorient_skip_mc_unwarp_anat_mean'
    merge_glob = conf.iproc.NAT111DIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    template_fname='NAT111_meanvol_allscansmean.nii.gz'
    template = os.path.join(conf.template.TEMPLATE_DIR,template_fname)

    ## adapted from ./QC/iProc_QC_T1nativespace_meanBOLDS.py
    job_spec_list = steps.fslmerge_meantime(template,globfiles_tomerge,overwrite=args.overwrite)
    logger.info('merging BOLD scans')
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_anat_mean'])

    # NAT111_meanvol
    def nat_mean_taskname(sessionid,bold_no,merge_filter,midvol_pad):
        spacename = "%s_bld%s_%s" % (sessionid,bold_no,merge_filter)
        return spacename
    header_images={}
    header_images['t1'] = mpr_reorient
    header_images['template'] = template

    anat_ax_slicer = {'window_dims':'30 220 30 220 44 128'.split(' '),
                        # <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
                        'sample': '2', # todo: what is this
                        'width': 15} # Number of images across, I think
    anat_sag_slicer = {'window_dims':'30 220 30 220 44 128'.split(' '),
                        'sample': '2', # sample every 'n'th images
                        'width': 16} # w*180 = max width
    outdir = conf.iproc.NAT111DIR
    job_spec_list = QC_warp(steps, outdir,merge_filter,header_images,anat_ax_slicer,anat_sag_slicer,nat_mean_taskname, args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['anat_mean_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]

    # NAT111_midvol
    # midvol_out from combine_warps_post
    merge_filter='reorient_skip_mc_unwarp_anat_vol'
    merge_glob = conf.iproc.NAT111DIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}???.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    template_fname = 'NAT111_midvol_allscansmean.nii.gz'
    template = os.path.join(conf.template.TEMPLATE_DIR,template_fname)

    ## adapted from ./QC/iProc_QC_T1nativespace_midvolBOLDS.py
    job_spec_list = steps.fslmerge_meantime(template,globfiles_tomerge,overwrite=args.overwrite)
    logger.info('merging BOLD scans')
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_anat_midvols'])

    def nat_mid_taskname(sessionid,bold_no,merge_filter,midvol_pad):
        spacename = "%s_bld%s_%s%s" % (sessionid,bold_no,merge_filter,midvol_pad)
        return spacename
    header_images={}
    header_images['t1'] = mpr_reorient
    header_images['template'] = template
    job_spec_list = QC_warp(steps, outdir,merge_filter,header_images,anat_ax_slicer,anat_sag_slicer,nat_mid_taskname, args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['anat_midvols_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]

    ####
    ## MNI
    # MNI111_meanvol
    merge_filter = 'reorient_skip_mc_unwarp_anat_mni_mean'
    merge_glob = conf.iproc.MNI111DIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    # from QC/iProc_QC_MNIspace_meanBOLDS.py
    template_fname='MNI111_meanvol_allscansmean.nii.gz'
    templatefile = os.path.join(conf.template.TEMPLATE_DIR,template_fname)

    job_spec_list = steps.fslmerge_meantime(templatefile,globfiles_tomerge,overwrite=args.overwrite)
    logger.info('merging BOLD scans')
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_mni_mean'])

    header_images={}
    header_images['t1'] = anat_mni_underlay
    header_images['template'] = templatefile
    mni_ax_slicer = {'window_dims':'0 170 0 200 30 140'.split(' '),
                        # <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
                        'sample': '2', # todo: what is this
                        'width': 14} # Number of images across, I think

    mni_sag_slicer = {'window_dims':'10 200 0 160 20 140'.split(' '),
                        'sample': '2', # sample every 'n'th images
                        'width': 16} # w*180 = max width
    def mean_task_spacename(sessionid,bold_no,merge_filter,midvol_pad):

        spacename = "%s_bld%s_%s" % (sessionid,bold_no,merge_filter)
        return spacename
    outdir = conf.iproc.MNI111DIR
    job_spec_list =  QC_warp(steps, outdir,merge_filter,header_images,mni_ax_slicer,mni_sag_slicer,mean_task_spacename, args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['mni_mean_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]
    logger.info('creating QC pdfs')

    # MNI111_midvol
    merge_filter='reorient_skip_mc_unwarp_anat_mni_vol'
    merge_glob = conf.iproc.MNI111DIR + '/{SESS}/{TASK}/{SESS}_bld???_{STEPS}???.nii.gz'.format(SESS='*',TASK='*',STEPS=merge_filter)
    globfiles_tomerge = get_glob(args,merge_glob)
    template_fname = 'MNI111_midvol_allscansmean.nii.gz'
    templatefile = os.path.join(conf.template.TEMPLATE_DIR,template_fname)

    job_spec_list = steps.fslmerge_meantime(templatefile,globfiles_tomerge,overwrite=args.overwrite)
    logger.info('merging BOLD scans')
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fslmerge_meantime_mni_midvols'])

    header_images={}
    header_images['t1'] = anat_mni_underlay
    header_images['template'] = templatefile
    #MNI111_midvol
    def mid_task_spacename(sessionid,bold_no,merge_filter,midvol_pad):
        spacename = "%s_bld%s_%s%s" % (sessionid,bold_no,merge_filter,midvol_pad)
        return spacename
    job_spec_list =  QC_warp(steps, outdir,merge_filter,header_images,mni_ax_slicer,mni_sag_slicer,mid_task_spacename, args.overwrite)
    rmfiles += execute(args.executor, job_spec_list,steps, **args.cluster['mni_midvols_qc'])
    qc_pdfs += [job_spec.outfiles for job_spec in job_spec_list]
    logger.info('creating QC pdfs')

    for qc_pdf in qc_pdfs:
        logger.info(qc_pdf)
    logger.info('done with warping! Check QC PDFs by flipping through and looking for major changes in the location of sulci and gyri')
    #os.rename(steps.rm_dump_filename,steps.rm_final_filename)
    # all produced rmfiles are consumed internally
    return rmfiles

def QC_warp(steps, outdir,merge_filter,header_images,ax_slicer,sag_slicer,task_spacename, overwrite):
    # set up qc objects
    qc_ax = qc.qc_pdf_maker(conf,'ax')
    qc_sag = qc.qc_pdf_maker(conf,'sag')
    # create the template pages
    template_img= header_images['template']
    # slicer params are the same as the meanvol section above.
    template_page_ax = qc.page(template_img,ax_slicer)
    template_page_sag = qc.page(template_img,sag_slicer)
    qc_ax.pages.append(template_page_ax)
    qc_sag.pages.append(template_page_sag)
    # create the T1 pages
    t1_img= header_images['t1']
    anat_page_ax = qc.page(t1_img,ax_slicer)
    qc_ax.pages.append(anat_page_ax)
    anat_page_sag = qc.page(t1_img,sag_slicer)
    qc_sag.pages.append(anat_page_sag)

    # create task pages
    for sessionid,_ in steps.scans.sessions():
            for task_type,bold_scan in steps.scans.tasks():
                scan_no = bold_scan['BLD']
                bold_no = "%03d" % int(scan_no)
                numvol = steps.scans.task_dict[task_type]['NUMVOL']
                midvol_num = str(int(numvol)/2)
                midvol_pad = midvol_num.zfill(3)
                spacename = task_spacename(sessionid,bold_no,merge_filter,midvol_pad)
                infile = os.path.join(outdir,sessionid,task_type,spacename+'.nii.gz')
                task_page_sag = qc.page(infile,sag_slicer)
                task_page_ax = qc.page(infile,ax_slicer)
                qc_sag.pages.append(task_page_sag)
                qc_ax.pages.append(task_page_ax)
    joblist = []
    ax_job = qc_ax.produce_pdf(merge_filter,save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if ax_job:
       joblist.append(ax_job) 
    sag_job = qc_sag.produce_pdf(merge_filter,save_intermediates=steps.args.no_remove_files,overwrite=overwrite)
    if sag_job:
       joblist.append(sag_job) 
    return joblist

def filter_and_project(steps,args):

    steps.load_rmfile_dump('unwarp_motioncorrect_align')
    rmfiles = []
    ########################################
    job_spec_list = steps.calculate_nuisance_params(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['calculate_nuisance_params'])
    ## can these be run all at the same time? I think so
    #MNI
    job_spec_list = steps.nuisance_regress('MNI111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['nuisance_regress_mni'])
    job_spec_list = steps.bandpass('MNI111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['bandpass_mni'])
    job_spec_list = steps.wholebrain_only_regress('MNI111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['wholebrain_only_regress_mni'])

    #T1
    job_spec_list = steps.nuisance_regress('NAT111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['nuisance_regress_anat'])
    job_spec_list = steps.bandpass('NAT111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['bandpass_anat'])
    job_spec_list = steps.wholebrain_only_regress('NAT111', overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['wholebrain_only_regress_anat'])

    # step 6
    job_spec_list = steps.fs6_project_to_surface(overwrite=args.overwrite)
    rmfiles += execute(args.executor,job_spec_list,steps,**args.cluster['fs6_project_to_surface']) #maybe 150GB

    logger.info('iProc completed')
    #os.rename(steps.rm_dump_filename,steps.rm_final_filename)
    # does not add any rmfiles
    return rmfiles

##
# helpers
##

def execute(executor_type, job_spec_list,steps, **kwargs):
    # short-circuit if step is set to "SKIP"
    if kwargs['RUNMODE'] == 'SKIP':
        logger.info('skipping jobs in job list')
        return []
    elif kwargs['RUNMODE'] == 'run':
        pass
    del kwargs['RUNMODE']
    logger.info('running jobs in job list')
    #TODO: put in rerun_on_fail for flaky steps
    skip_fail = steps.args.skip_fail
    # If I had to do again would make executor class to store this
    if steps.args.dry_run:
        for job in job_spec_list:
            job.dummy = True
    ## attach wrappers
    if steps.args.singularity:
        for j in job_spec_list:
            # check for empty command
            if j.skip:
                continue
            wrapper = os.path.join(conf.iproc.CODEDIR,'wrappers','singularity_wrap.sh')
            if steps.args.no_srun :
                srun = "NO"
            else:
                srun = "YES"
            def cpu(kwargs):
                try:
                    return str(kwargs['cpu'])
                except KeyError:
                    return str(1)
            #ncf specific, will have to abstract
            singularity_prefix = ['singularity', 'exec', '-e', steps.args.singularity, wrapper, conf.fs.SUBJECTS_DIR, srun, cpu(kwargs)]
            j.prepend_cmd(singularity_prefix)
            for dependent_job,_ in afterok_jobs(j):
                dependent_job.prepend_cmd(singularity_prefix)

    # non-singularity wrapper handling
    if steps.args.wrap:
        wrapper = os.path.join(conf.iproc.CODEDIR,'wrappers',steps.args.wrap)

    if steps.args.user_wrap:
        wrapper = steps.args.user_wrap

    if steps.args.user_wrap or steps.args.wrap:
        for j in job_spec_list:
            if j.skip:
                continue
            prefix = [wrapper]
            if steps.args.wrap_args:
                prefix = prefix+steps.args.wrap_args
            j.prepend_cmd(prefix)
            for dependent_job,_ in afterok_jobs(j):
                dependent_job.prepend_cmd(prefix)

    try:
        if executor_type == 'local':
            for job in job_spec_list:
                # check for empty command
                if job.skip:
                    continue
                if job.dummy:
                    logger.info(job.cmd)
                    continue
                timestamp = time.time()
                hostname = sp.check_output(['hostname']).strip()
                job.stdout = job.logfile_base +'_{}_{}.err'.format(hostname,timestamp)
                job.stdout = job.logfile_base +'_{}_{}.err'.format(hostname,timestamp)
                stdout = open(job.stdout, 'w')
                stderr = open(job.stderr, 'w')
                logger.debug(job.cmd)
                # run cmd, throw error if it fails
                try:
                    logger.debug('running {}'.format(job.cmd))
                    commons.check_call(job.cmd, stdout=stdout, stderr=stderr)
                    job.state = 'COMPLETED' 
                except Exception:
                    logger.info('local script has failed: {}'.format(e))
                    logger.info(job.stdout)
                    logger.info(job.stderr)
                    stdout.close()
                    sterr.close()
                    if skip_fail:
                        continue
                    raise commons.FailedJobError(job)

        elif executor_type in ['slurm','pbsubmit']:
            if executor_type=='slurm':
                if steps.args.sbatch_args: 
                    sbatch_arg_dict = {str(x):y for x,y in zip(range(len(steps.args.sbatch_args)),steps.args.sbatch_args)}
                    kwargs.update(sbatch_arg_dict)
            if executor_type == 'pbssubmit':
                if steps.args.interval > 2:
                    logger.warn('PBS forgets completed jobs every 2 minutes or so. Choose a shorter polling interval')
                    logger.warn('On the Martinos center, ')
            executor = executors.get(executor_type)
            if not job_spec_list:
                logger.debug('empty job_spec_list')
                return []
            throttle_no = kwargs.get('throttle', False)
            if throttle_no:
                job_limit = throttle_no
            else:
                job_limit = float('inf')
            if steps.args.single_file:
                #never mind the above, set to 1
                job_limit = 1
            jobspec_kwargs=[]
            for job_spec in job_spec_list:
                # check for empty command
                kwargs_prep(kwargs,steps.args)
                jobspec_kwargs.append((job_spec,kwargs))
                if job_spec.afterok:
                    for dependent_pair in job_spec.afterok:
                        # for now, dependent jobs only play nice with throttling when there is one per job.
                        dependent_job,afterok_kwargs=dependent_pair
                        # skip afterOK part if parent job is a skipped job
                        kwargs_prep(afterok_kwargs,steps.args)
                        jobspec_kwargs.append((dependent_job,afterok_kwargs))
            if jobspec_kwargs:
                if skip_fail:
                    cancel=False
                else:
                    cancel=True

                pickle_file = os.path.join(conf.iproc.RMFILE_DUMP, "save.p")
                pickle.dump( jobspec_kwargs, open(pickle_file, "wb" ) )                
                executors.rolling_submit(executor,jobspec_kwargs,job_limit,steps.args.interval,cancel_on_fail=cancel)
        else:
            raise NotImplementedError('executor type {} is not supported'.format(executor_type))
    except commons.FailedJobError as e:
        post_process_jobs(job_spec_list)
        raise e
    post_process_jobs(job_spec_list)
    rmfiles = rmfiles_from_job_specs(job_spec_list)
    if rmfiles:
        return rmfiles
    else:
        # so += works wherever we're calling this function from
        return []

def afterok_jobs(j):
    if j.afterok:
        for dependent_pair in j.afterok:
            dependent_job,kwargs = dependent_pair
            while dependent_job:
                yield(dependent_job,kwargs)
                if dependent_job.afterok:
                    next_level_job,next_kwargs = dependent_job.afterok
                    dependent_job = next_level_job
                    kwargs = next_kwargs
                else:
                    break

def post_process_jobs(job_spec_list):
    # process job list after it has been run through an executor
    # compile all jobs and dependencies
    jobs=[]
    for job in job_spec_list:
        jobs.append(job)
        for j,_ in afterok_jobs(job):
            jobs.append(j)

    for job in jobs:
        logger.debug(vars(job))
        if job.state == 'COMPLETED':
            # copy outfiles to destination directory, append rmfiles to object and file.
            logger.debug('execution finished successfully. Produced the following files:')
            logger.debug(job.outfiles)
            # job succeeded, copy logfile
            # TODO: this may not work for pbsubmit.
            # The job.stdout must be an existing valid file path.
            for outdir in job.outfile_dirs:
                if not os.path.isdir(outdir):
                    os.makedirs(outdir)
                shutil.copy2(job.stdout, outdir)
                shutil.copy2(job.stderr, outdir)

def rmfiles_from_job_specs(l):
    # takes in a list of JobSpecs, returns flat list or rmfiles.
    rmfile_list = []
    for job in l:
        for dependent_job,_ in afterok_jobs(job):
            if dependent_job.rmfiles:
                logger.debug(dependent_job.rmfiles)
                rmfile_list += dependent_job.rmfiles

        if job.rmfiles:
            logger.debug(job.rmfiles)
            for item in job.rmfiles:
                rmfile_list.append(item)
    return rmfile_list

def kwargs_prep(kwargs,args):
    try:
        assert(kwargs['partition'])
    except KeyError: #set default
        kwargs['partition'] = args.queue
    if args.nodelist:
        kwargs['nodelist'] = args.nodelist
    if args.exclude:
        kwargs['exclude'] = args.exclude
    if 'throttle' in kwargs:
        del kwargs['throttle']
 
def get_glob(args,merge_glob):
    globfiles_tomerge = glob.glob(merge_glob)
    if not globfiles_tomerge:
        logger.error('failed glob {}'.format(merge_glob))
        if not args.dry_run:
            raise IOError('failed glob {}'.format(merge_glob))
    return globfiles_tomerge

def configure_logging(level):

    logdir = conf.iproc.logdir
    if not os.path.exists(logdir):
        os.makedirs(logdir)
    time.sleep(1)
    now = dt.datetime.now().strftime('%y%b%d_%H%m%S')
    log_fname = '{fn}_{sub}_{sess}_{datetime}.log'.format(fn='iproc',
                                                          sub=conf.iproc.sub,
                                                          sess=conf.template.midvol_sess,
                                                          datetime=now)
    log_path = os.path.join(logdir, log_fname)

    log_format = '[%(asctime)s][%(levelname)s] - %(name)s - %(message)s'

    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'console': {
                'format': log_format
            },
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'formatter': 'console',
            },
            'file': {
                'class': 'logging.FileHandler',
                'formatter': 'console',
                'filename': log_path
            },
        },
        'loggers': {
        # root logger
            '': {
                'level': level,
                'handlers': ['console', 'file'],
            },
        },
    })
    return log_path

def log_provenance(conf_filename,log_path):
    git_repo = os.path.join(conf.iproc.CODEDIR, '.git')
    cmd = [
        'git',
        '--git-dir', git_repo,
        'rev-parse', 'HEAD'
    ]
    commit_hash = sp.check_output(cmd).strip()
    logger.info(commons.program(sys.argv))
    logger.info(commons.machine())
    logger.info('git checksum: {}'.format(commit_hash))
    logger.info('conf filename: {}'.format(os.path.abspath(conf_filename)))
    logger.info(conf.items())
    logger.info('logfile: {}'.format(log_path))


phraseList=[
    ('setup',setup),
    ('bet',check_bet),
    ('unwarp_motioncorrect_align',unwarp_motioncorrect_align),
    ('T1_warp_and_mask',T1_warp_and_mask),
    ('combine_and_apply_warp',combine_and_apply_warp),
    ('filter_and_project',filter_and_project)
]
stages = collections.OrderedDict(phraseList)

def main():
    # define args
    parser = ap.ArgumentParser(usage='%(prog)s -c <configfile> -s {setup,bet,unwarp_motioncorrect_align,T1_warp_and_mask,combine_and_apply_warp,filter_and_project} \n %(prog)s -h for full option listing')

    # global/universal args
    required = parser.add_argument_group('REQUIRED')
    required.add_argument('-c', '--config-file', required=True,
        help='Subject configuration file')
    required.add_argument('-s', '--stage', required=True,
        help='stage of analysis to carry out', choices=stages.keys() )
    parser.add_argument('--debug', action='store_true',
        help='Enable debug messages and save all intermediate files')
    parser.add_argument('--executor', choices=('local', 'slurm', 'pbsubmit'),
        default='slurm')

    job_handling = parser.add_argument_group('job handling behavior options')
    job_handling.add_argument('--no-remove-files', action='store_true',
        help='generate shell script of files to remove, rather than the default, which is to automatically remove files at the earliest opportunity.')
    job_handling.add_argument('--overwrite', action='store_true',
        help='overwrite files from prior runs. Default is to skip reruns of jobs that have already produced output files.')

    ## edge case handlers
    parser.add_argument('--blank-rmfiles', action='store_true',
        help='if no rmfiles dump is found from a prior step, ignore and load blank. You should only need to use this if you see some error mentioning rmfiles.')

    ## alternate use cases
    parser.add_argument('--bids',
        help='instead of downloading from XNAT, download from specified BIDS directory')

    ## executor modifiers
    executor = parser.add_argument_group('executor modifier options') 
    executor.add_argument('-q', '--queue', default='ncf_holy',
        help='default slurm/pbs partition to submit to')
    executor.add_argument('-i', '--interval', default=5, type=int,
        help='check children jobs once every i minutes')
    executor.add_argument('--dry-run', action='store_true',
        help='dry-run mode (no scripts are actually run)')
    executor.add_argument('--skip-fail', action='store_true',
        help='if an individual job fails, try to finish the stage. If this is not set, behavior is to cancel all running jobs and halt')
    executor.add_argument('--single-file', action='store_true',
        help='Only run one job at a time')

    ## Mostly slurm-specific
    
    slurm = parser.add_argument_group('slurm-specific modifier options') 
    slurm.add_argument('--exclude',
        help='list of nodes to exclude from slurm run, usage --exclude "holy2a06201,holy2a06202" ')
    slurm.add_argument('--nodelist',
        help='list of nodes to run on for slurm run, usage --nodelist "holy2a06201,holy2a06202" ')
    slurm.add_argument('--sbatch-args', nargs='+', 
        help='a way to pass in arguments to sbatch, if needed.\n Will be applied to all sbatch commands.\n each argument must be a single string without spaces, and missing the initial dashes, e.g. --sbatch-args "chdir=<directory>" (which would correspond to `sbatch --chdir=<directory>` in regular command line usage of sbatch.)')

    ## wrappers
    wrappers = parser.add_argument_group('wrapper options') 
    wrappers.add_argument('--singularity',
        help='run job commands through centos6 singularity container. \n on the NCF, you should pass this /n/sw/singularity/fasrc-compute-1.0.el6.img. Should be used together with --no-srun, as srun use within singularity containers is typically problematic.')
    # TODO: implement Fra's workaround to submit srun/squeue from within the container
    wrappers.add_argument('--no-srun', action='store_true',
        help='environment is not safe to run srun. Typically only used in conjunction with singularity and slurm')
    wrappers.add_argument('--wrap',
        help='run job commands through wrapper. \n current options are profiling/du.sh, profiling/mountinfo.sh, and profiling/nfsstat_before_after.sh.')
    wrappers.add_argument('--user-wrap',
        help='run job commands through user-specified wrapper. \n Make sure to use the full path to your wrapper script.')
    wrappers.add_argument('--wrap-args', nargs='+',
        help='a way to pass in arguments to your wrapper script, if needed')
    # end of argparser spec
    ##

    # set up args
    args = parser.parse_args()
    if args.sbatch_args:
        args.sbatch_args = ['--{}'.format(arg) for arg in args.sbatch_args]

    # parse the configuration file
    conf.parse(os.path.expanduser(args.config_file))
    # set variables with fully deterministic locations
    allowed_fmap_regimes = ['topup','fsl_prepare_fieldmap']
    if not conf.fmap.preptool in allowed_fmap_regimes:
        logger.error('{} must have a fmap:preptool value matching one of {}'.format(args.config_file,allowed_fmap_regimes))
        exit(1)

    conf.iproc.WORKDIR = os.path.join(conf.iproc.SCRATCHDIR,str(int(time.time())))
    #commons.move_on_exit(conf.iproc.WORKDIR, conf.iproc.OUTDIR)
    # note if this is local, rather than networked scratch, this may not work
    if not os.path.isdir(conf.iproc.WORKDIR):
        os.makedirs(conf.iproc.WORKDIR)
    

    # construct outdir shortcuts
    conf.iproc.LOGDIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'logs')
    conf.iproc.QCDIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'QC')
    conf.iproc.NATDIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'NAT')
    conf.iproc.MNI111DIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'MNI111')
    conf.iproc.NAT111DIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'NAT111')
    conf.iproc.FS6DIR= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'FS6')
    conf.iproc.RMFILE_DUMP= os.path.join(conf.iproc.OUTDIR,conf.iproc.SUB,'rmfiles')
    if not os.path.isdir(conf.iproc.RMFILE_DUMP):
        os.makedirs(conf.iproc.RMFILE_DUMP)

    # make sure template dir exists
    template_dir = os.path.join(conf.iproc.OUTDIR, conf.iproc.SUB, 'cross_session_maps', 'templates')
    conf.template.TEMPLATE_DIR = template_dir
    if not os.path.exists(template_dir):
        os.makedirs(template_dir)
    conf.template.midvols_mean = os.path.join(conf.template.TEMPLATE_DIR,'{}midmean_midvols_on_midvoltarg.nii.gz'.format(conf.iproc.SUB))

    # get environmental variables from module system (NCF specific)
    #TODO: is this the best way to do this?
    try:
        foo = conf.iproc.CODEDIR
    except ConfigError:
        conf.iproc.CODEDIR = os.environ['_IPROC_CODEDIR']

    # set environmental variables

        # for freesurfer
    os.environ['SUBJECTS_DIR'] = conf.fs.SUBJECTS_DIR
        # for iProc
    if args.no_srun :
        os.environ['IPROC_SRUN'] = "NO"
    else:
        os.environ['IPROC_SRUN'] = "YES"

    # configure logging
    level = logging.DEBUG if args.debug else logging.INFO
    log_path = configure_logging(level)
    # log provenance for this program
    log_provenance(args.config_file, log_path)

    # open and parse CSVs
    scans = csvHandler.scansHandler(conf)
    scans.ingest_task_csv(conf.csv.TASKTYPELIST)
    
    scans.ingest_bold_csv(conf.csv.SCANLIST)
    midvol_boldno = int(conf.template.MIDVOL_BOLDNO)

    # gross but prevents errors caused by user typos
    conf.template.MIDVOL_BOLDNAME = scans.scan_by_session[conf.template.MIDVOL_SESS].bold_scans[midvol_boldno]['TYPE']
    
    args.cluster = {}
    csvHandler.load_cluster_requests(conf.csv.CLUSTER_REQUESTS,args)
    
    # copy CSVs to log directory so we have permanent record
    csv_archive = log_path.rstrip('log') + 'csv_cfg_archive'
    os.makedirs(csv_archive)
    shutil.copy2(conf.csv.TASKTYPELIST,csv_archive)
    shutil.copy2(conf.csv.SCANLIST,csv_archive)
    shutil.copy2(conf.csv.CLUSTER_REQUESTS,csv_archive)
    shutil.copy2(args.config_file,csv_archive)

    if args.bids:
        # add 'BIDS_ID' attribute
        bids.match_scan_no_to_bids(args.bids,scans)

    # run the stage specified by the user
    steps = iProcSteps.jobConstructor(conf,scans,args)
    stage = stages.get(args.stage)
    
    rm_list = stage(steps,args)

    rmscript_location = os.path.join(conf.iproc.LOGDIR,'rm_{}.sh'.format(args.stage))
    rm_script = commons.ScriptBuilder(rmscript_location)
    rm_script.check_header()
    for rmfile in rm_list:
        rm_script.append(['rm','-rf', rmfile])
    logger.info('in order to remove files, run {}'.format(rmscript_location))

if __name__ == '__main__':
    main()
